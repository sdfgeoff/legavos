All the stuff I created is released under CC-BY-SA, so you can do what you like with it,
but if you share it you should attribute myself as the original source.
