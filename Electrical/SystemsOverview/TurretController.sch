EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5EAAAA7F
P 9800 1850
F 0 "J?" H 9880 1892 50  0000 L CNN
F 1 "TiltServo" H 9880 1801 50  0000 L CNN
F 2 "" H 9800 1850 50  0001 C CNN
F 3 "~" H 9800 1850 50  0001 C CNN
	1    9800 1850
	1    0    0    -1  
$EndComp
$Comp
L MCU_Module:Arduino_Nano_v2.x A?
U 1 1 5EAAB561
P 5600 2100
F 0 "A?" H 5600 1011 50  0000 C CNN
F 1 "Arduino_Nano_v2.x" H 5600 920 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 5600 2100 50  0001 C CIN
F 3 "https://www.arduino.cc/en/uploads/Main/ArduinoNanoManual23.pdf" H 5600 2100 50  0001 C CNN
	1    5600 2100
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:DRV8833PW U?
U 1 1 5EAAC838
P 8550 4750
F 0 "U?" H 8550 3961 50  0000 C CNN
F 1 "DRV8833PW" H 8550 3870 50  0000 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 9000 5200 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/drv8833.pdf" H 8400 5300 50  0001 C CNN
	1    8550 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EAB385D
P 1600 1700
F 0 "#PWR?" H 1600 1450 50  0001 C CNN
F 1 "GND" H 1605 1527 50  0000 C CNN
F 2 "" H 1600 1700 50  0001 C CNN
F 3 "" H 1600 1700 50  0001 C CNN
	1    1600 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 1700 1600 1700
Text Notes 9050 4500 0    50   ~ 0
Currently this is on a driver board. \nThe pins that are marked NC are ones\nnot broken out on the driver board.
NoConn ~ 8750 4050
NoConn ~ 7950 4550
NoConn ~ 7950 4650
NoConn ~ 7950 4450
Wire Wire Line
	8700 1850 9600 1850
$Comp
L power:GND #PWR?
U 1 1 5EAC008A
P 9100 1950
F 0 "#PWR?" H 9100 1700 50  0001 C CNN
F 1 "GND" H 9105 1777 50  0000 C CNN
F 2 "" H 9100 1950 50  0001 C CNN
F 3 "" H 9100 1950 50  0001 C CNN
	1    9100 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 1950 9600 1950
Text HLabel 9200 1750 0    50   Output ~ 0
tilt_servo
Text HLabel 4950 1800 0    50   Output ~ 0
tilt_servo
Wire Wire Line
	4950 1800 5100 1800
Wire Wire Line
	5500 900  5500 1100
$Comp
L power:GND #PWR?
U 1 1 5EACC2F6
P 5700 3500
F 0 "#PWR?" H 5700 3250 50  0001 C CNN
F 1 "GND" H 5705 3327 50  0000 C CNN
F 2 "" H 5700 3500 50  0001 C CNN
F 3 "" H 5700 3500 50  0001 C CNN
	1    5700 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3500 5700 3100
$Comp
L power:+BATT #PWR?
U 1 1 5EACD720
P 8850 3750
F 0 "#PWR?" H 8850 3600 50  0001 C CNN
F 1 "+BATT" H 8865 3923 50  0000 C CNN
F 2 "" H 8850 3750 50  0001 C CNN
F 3 "" H 8850 3750 50  0001 C CNN
	1    8850 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 3750 8850 4050
$Comp
L power:GND #PWR?
U 1 1 5EACE02B
P 8550 5750
F 0 "#PWR?" H 8550 5500 50  0001 C CNN
F 1 "GND" H 8555 5577 50  0000 C CNN
F 2 "" H 8550 5750 50  0001 C CNN
F 3 "" H 8550 5750 50  0001 C CNN
	1    8550 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 5450 8550 5750
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5EACE8E8
P 9850 4850
F 0 "J?" H 9930 4842 50  0000 L CNN
F 1 "LoaderMotor" H 9930 4751 50  0000 L CNN
F 2 "" H 9850 4850 50  0001 C CNN
F 3 "~" H 9850 4850 50  0001 C CNN
	1    9850 4850
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5EACED97
P 9850 5250
F 0 "J?" H 9930 5242 50  0000 L CNN
F 1 "GunMotor" H 9930 5151 50  0000 L CNN
F 2 "" H 9850 5250 50  0001 C CNN
F 3 "~" H 9850 5250 50  0001 C CNN
	1    9850 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 4850 9650 4850
Wire Wire Line
	9150 4950 9650 4950
Wire Wire Line
	9150 5050 9450 5050
Wire Wire Line
	9450 5050 9450 5250
Wire Wire Line
	9450 5250 9650 5250
Wire Wire Line
	9150 5150 9350 5150
Wire Wire Line
	9350 5150 9350 5350
Wire Wire Line
	9350 5350 9650 5350
Text HLabel 7650 5050 0    50   Input ~ 0
gun_motor_1
Wire Wire Line
	7650 5050 7950 5050
Text HLabel 4750 2000 0    50   Input ~ 0
gun_motor_1
Wire Wire Line
	4750 2000 5100 2000
Text HLabel 4750 2100 0    50   Input ~ 0
gun_motor_2
Wire Wire Line
	4750 2100 5100 2100
Text HLabel 7650 5150 0    50   Input ~ 0
gun_motor_2
Wire Wire Line
	7650 5150 7950 5150
Text HLabel 4750 2400 0    50   Input ~ 0
loader_motor_1
Text HLabel 4750 2500 0    50   Input ~ 0
loader_motor_2
Wire Wire Line
	4750 2400 5100 2400
Wire Wire Line
	4750 2500 5100 2500
Text HLabel 7650 4850 0    50   Input ~ 0
loader_motor_1
Text HLabel 7650 4950 0    50   Input ~ 0
loader_motor_2
Wire Wire Line
	7650 4850 7950 4850
Wire Wire Line
	7650 4950 7950 4950
Text Notes 7950 7550 0    100  ~ 20
Turret Electronics
$Comp
L Connector_Generic:Conn_01x05 J?
U 1 1 5EAF2FE2
P 2300 6250
F 0 "J?" H 2218 5825 50  0000 C CNN
F 1 "Video Transmitter" H 2218 5916 50  0000 C CNN
F 2 "" H 2300 6250 50  0001 C CNN
F 3 "~" H 2300 6250 50  0001 C CNN
	1    2300 6250
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5EAF495B
P 4900 6200
F 0 "J?" H 4980 6242 50  0000 L CNN
F 1 "Camera" H 4980 6151 50  0000 L CNN
F 2 "" H 4900 6200 50  0001 C CNN
F 3 "~" H 4900 6200 50  0001 C CNN
	1    4900 6200
	1    0    0    -1  
$EndComp
Text HLabel 2850 6050 2    50   Output ~ 0
camera_ground
Wire Wire Line
	2850 6050 2500 6050
Text HLabel 2850 6150 2    50   Output ~ 0
video
Text HLabel 2850 6250 2    50   Output ~ 0
audio
Wire Wire Line
	2850 6250 2500 6250
$Comp
L power:GND #PWR?
U 1 1 5EB00197
P 2900 6350
F 0 "#PWR?" H 2900 6100 50  0001 C CNN
F 1 "GND" H 2905 6177 50  0000 C CNN
F 2 "" H 2900 6350 50  0001 C CNN
F 3 "" H 2900 6350 50  0001 C CNN
	1    2900 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 6350 2500 6350
$Comp
L power:+BATT #PWR?
U 1 1 5EB03BDC
P 2900 6850
F 0 "#PWR?" H 2900 6700 50  0001 C CNN
F 1 "+BATT" H 2915 7023 50  0000 C CNN
F 2 "" H 2900 6850 50  0001 C CNN
F 3 "" H 2900 6850 50  0001 C CNN
	1    2900 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 6850 2650 6850
Wire Wire Line
	2650 6850 2650 6450
Wire Wire Line
	2650 6450 2500 6450
$Comp
L power:+BATT #PWR?
U 1 1 5EB0670F
P 4100 6100
F 0 "#PWR?" H 4100 5950 50  0001 C CNN
F 1 "+BATT" H 4115 6273 50  0000 C CNN
F 2 "" H 4100 6100 50  0001 C CNN
F 3 "" H 4100 6100 50  0001 C CNN
	1    4100 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 6100 4700 6100
Text HLabel 4400 6300 0    50   Output ~ 0
video
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5EB3031B
P 1150 1600
F 0 "J?" H 1068 1275 50  0000 C CNN
F 1 "FromBody" H 1068 1366 50  0000 C CNN
F 2 "" H 1150 1600 50  0001 C CNN
F 3 "~" H 1150 1600 50  0001 C CNN
	1    1150 1600
	-1   0    0    1   
$EndComp
$Comp
L Device:D D?
U 1 1 5EB39F35
P 8550 1850
F 0 "D?" H 8550 1634 50  0000 C CNN
F 1 "D" H 8550 1725 50  0000 C CNN
F 2 "" H 8550 1850 50  0001 C CNN
F 3 "~" H 8550 1850 50  0001 C CNN
	1    8550 1850
	-1   0    0    1   
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 5EB3A9CB
P 2600 1600
F 0 "#PWR?" H 2600 1450 50  0001 C CNN
F 1 "+BATT" H 2615 1773 50  0000 C CNN
F 2 "" H 2600 1600 50  0001 C CNN
F 3 "" H 2600 1600 50  0001 C CNN
	1    2600 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 1600 1350 1600
$Comp
L power:+BATT #PWR?
U 1 1 5EB41F79
P 8200 1750
F 0 "#PWR?" H 8200 1600 50  0001 C CNN
F 1 "+BATT" H 8215 1923 50  0000 C CNN
F 2 "" H 8200 1750 50  0001 C CNN
F 3 "" H 8200 1750 50  0001 C CNN
	1    8200 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 1750 8200 1850
Wire Wire Line
	8200 1850 8400 1850
$Comp
L power:+BATT #PWR?
U 1 1 5EB508B1
P 5500 900
F 0 "#PWR?" H 5500 750 50  0001 C CNN
F 1 "+BATT" H 5515 1073 50  0000 C CNN
F 2 "" H 5500 900 50  0001 C CNN
F 3 "" H 5500 900 50  0001 C CNN
	1    5500 900 
	1    0    0    -1  
$EndComp
Text Notes 8150 1450 0    50   ~ 0
The servo can accept a maximum of 7 Volts. \nA lipo can idle as high as 8.4V, so some step\ndown is needed. In the main electronics there\nis a single big diode, which drops the\nmaximum voltage to 7.7V. This additional\ndiode drops the voltage to 7 V\n\nI used a FR155 because it's what I had
Wire Wire Line
	9200 1750 9600 1750
$Comp
L power:GND #PWR?
U 1 1 5EB5A963
P 4000 6200
F 0 "#PWR?" H 4000 5950 50  0001 C CNN
F 1 "GND" H 4005 6027 50  0000 C CNN
F 2 "" H 4000 6200 50  0001 C CNN
F 3 "" H 4000 6200 50  0001 C CNN
	1    4000 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 6200 4700 6200
Wire Wire Line
	4400 6300 4700 6300
Wire Wire Line
	2850 6150 2500 6150
Text GLabel 1600 1500 2    50   Input ~ 0
communication_bus
Wire Wire Line
	1350 1500 1600 1500
Text GLabel 4300 1100 0    50   Input ~ 0
communication_bus
Wire Wire Line
	5100 1500 4900 1500
Wire Wire Line
	4900 1500 4900 1100
Wire Wire Line
	4300 1100 4900 1100
$EndSCHEMATC
