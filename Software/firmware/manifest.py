""" This file is used to control what modules are baked into the firmware and
loaded onto the microcontroller """


# From original manifest
freeze("$(PORT_DIR)/modules")
#freeze("$(MPY_DIR)/tools", ("upip.py", "upip_utarfile.py"))
#freeze("$(MPY_DIR)/ports/esp8266/modules", "ntptime.py")
#freeze("$(MPY_DIR)/drivers/dht", "dht.py")
#freeze("$(MPY_DIR)/drivers/onewire")
#include("$(MPY_DIR)/extmod/webrepl/manifest.py")


# Our python:
freeze("python_modules", "boot.py")
freeze("python_modules", "log.py")
freeze("python_modules", "mainloop.py")
freeze("python_modules", "router.py")
freeze("python_modules", "topics.py")
freeze("python_modules", "version.py")
freeze("python_modules", "telemetry.py")

freeze("python_modules", "modules/log_printer.py")
freeze("python_modules", "modules/router_log_bridge.py")
freeze("python_modules", "modules/servo.py")
freeze("python_modules", "modules/udp_bridge.py")
freeze("python_modules", "modules/udp_heartbeat.py")
freeze("python_modules", "modules/wifi_network_join.py")
freeze("python_modules", "modules/wifi_network_host.py")
freeze("python_modules", "modules/gait_machine.py")
freeze("python_modules", "modules/turret.py")
freeze("python_modules", "modules/dynamixel_bus.py")
freeze("python_modules", "modules/dynamixel_servo.py")
freeze("python_modules", "modules/gun.py")





