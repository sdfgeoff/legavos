""" Define all the messages that can be passed around the system """
from collections import namedtuple
import json
import struct

SERIALIZERS = {}  # Maps from a topic to a function that serializes the data
DESERIALIZERS = {}  # Maps from a topic to a function that deserializes the data
TYPE_IDS = {}  # Maps from a topic to a type ID
TYPE_ID_INVERSE = {}  # Maps from a type ID to a topic constructor
TOPIC_LIST = []

def struct_packing(fmt):
    """ Serialization method for topics using a struct format string. This is
    fast but rigidly defines the message format and struggles with
    strings of non fixed length. Still, this is the preferrable method"""
    return (
        lambda message: struct.pack(fmt, *message),
        lambda message: struct.unpack(fmt, message)
    )


def json_packing():
    """ Serialization method for topics: use json. This is flexible but
    is slow to construct on embedded hardware. If your topic has
    a fixed structure (aka most of them), use struct_packing """
    return (
        lambda message: json.dumps(message).encode("utf-8"),
        lambda message: json.loads(message)
    )


def topic(name, fields, serializers):
    """ Create a new topic with the specified name, array of files and
    serialization methods """
    topic = namedtuple(name, fields)
    SERIALIZERS[topic] = serializers[0]
    DESERIALIZERS[topic] = serializers[1]

    type_id = len(TYPE_IDS)
    TYPE_IDS[topic] = type_id
    TYPE_ID_INVERSE[type_id] = topic

    TOPIC_LIST.append(topic)
    return topic



# --------------------------------- TOPICS ------------------------------------

GUN_MODE_INACTIVE = 0
GUN_MODE_ARMED = 1
GUN_MODE_UNLOAD = 2
GUN_MODE_LOAD = 3

# Used for normal communication with the ground station
ControlPacket = topic("ControlPacket", (
    "packet_count",  # Packet counter to track packet loss
    "chassis_forwards",  # forwards velocity as a percent of maximum
    "chassis_strafe",  # sideways velocity as a percent of maximum
    "chassis_rotate",  # angular velocity as a percent of maximum

    "turret_pan",  # angle of the turret relative to the chassis in 0.1 degrees increments
    "turret_elevation",  # tilt of the turret relative to horizontal in 0.1 degree increments

    # gun_mode sets if the gun is capable of firing or not.
    # the gun will fire if the gun_mode is GUN_MODE_ARMED and
    # if the gun_bullet_id changes.
    "gun_mode",  # One of the "GUN_MODE" variables
    "gun_bullet_id",  # This is an ID to ensure shots can handle latency/loss
), struct_packing("BbbbhhBB"))
Telemetry = topic("Telemetry", ["level", "key", "value"], struct_packing("bbi"))


# Emitted by the gait machine, used to correct for body swing while walking
WalkCycleBodyAngle = topic("WalkCycleBodyAngle", ("degrees"), struct_packing("f"))

# Commands to actual leg servos
CommandLegSelectServo = topic("CommandLegSelectServo", ("degrees"), struct_packing("f"))
CommandLegRightServo = topic("CommandLegRightServo", ("degrees"), struct_packing("f"))
CommandLegLeftServo = topic("CommandLegLeftServo", ("degrees"), struct_packing("f"))
CommandTurretPanServo = topic("CommandTurretPanServo", ("degrees"), struct_packing("f"))
CommandTurretTiltServo = topic("CommandTurretTiltServo", ("degrees"), struct_packing("f"))


DynamixelMessage = topic("DynamixelMessage", ("address", "instruction", "parameters"), json_packing())


# Fundamental to router system
PollUpdate = topic("PollUpdate", ("deltatime"), struct_packing("f"))
StartInstance = topic("StartInstance", ("name"), json_packing())
StopInstance = topic("StopInstance", ("name"), json_packing())
MessageMishandled = topic("MessageMishandled", ("instance_name", "traceback", "message"), json_packing())
StartInstanceFailed = topic("StartInstanceFailed", ("instance_name", "traceback"), json_packing())

Log = topic("Log", ["level", "message", "fields"], json_packing())


# Functions to serialize and deserialize topics
def serialize(event):
    """ write a file into a filelike-object so that it can be transmitted """
    topic_type = type(event)
    assert topic_type in SERIALIZERS
    assert topic_type in TYPE_IDS

    type_id = TYPE_IDS[topic_type]
    data = SERIALIZERS[topic_type](event)
    return struct.pack("b", type_id) + data


def deserialize(data):
    """ Attempts to deserialize data into a topic. Either returns a topic or
    None if deserializing failed"""
    type_id = data[0]

    if type_id not in TYPE_ID_INVERSE:
        return None
    topic_type = TYPE_ID_INVERSE[type_id]
    deserializer = DESERIALIZERS[topic_type]
    try:
        plain_data = deserializer(data[1:])  # Construct a tuple
        return topic_type(*plain_data)  # Construct a namedtuple
    except Exception as err:
        return None
