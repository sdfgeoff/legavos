""" A publisher/subscriber and module management system

The basic idea is to keep modules as separate from each other as possible.
This means that modules do not call into each others code, but are separated
by a messaging system. This messaging system contains topics (defined in
topics.py), to which an instance can publish and to which it can subscribe.

A module is a bunch of behaviour (eg a servo), and an instance is a single
variant of that module (eg a servo attached to pin 19). Instances are
ephermeral, and if an instance produces an error while handling a message it
will be destroyed and restarted. CURRENTLY THIS DOESN"T INCLUDE HANDLING
BLOCKING FUNCTIONS, SO STILL BE A BIT CAREFUL.

Because the router has to be able to create instances from modules, you need
to give it a factory function. This is done using the `register_instance`
function. This funtion is treated opaquely, so it could be a class type or a
lambda that constructs a class or an actual function.

Because instance are ephemeral, state referring to a an instance should not
be stored outside it. For this reason the topics an instance is sibscribed to
are static and are also defined in the `register_instance` function. There is
no way to dynamically change the subscriptions.


PLANS:
 - Remove recursion by shifting calls to "publish" to simply append to a queue
   This will make performance analysis easier
 - Make calls into signals faster. Currently there are a whole bunch of
   string lookup which are depressingly slow. Something like 20% of the CPU
   time is currently spent in these string lookups.

"""
import collections
import sys
import io
import topics as t


if sys.implementation.name == "micropython":
    PRINT_EXCEPTION = sys.print_exception # pylint: disable=no-member
    DEQUE = collections.deque
    import micropython
else:
    import traceback
    PRINT_EXCEPTION = lambda _, x: traceback.print_exc(file=x)
    DEQUE = lambda iterable, length, _flags: collections.deque(iterable, length)

    class micropython:
        def native(x):
            return x


# An InstanceDef contains all the details needed to create and subscribe an
# instance. spawn_function is the function to create an instance, signals
# is an map of {topic: function_name}
InstanceDef = collections.namedtuple("InstanceDef", ["spawn_function", "signals"])


def reset_system(_):
    """ Run when the router is restarted """
    # Sometimes it's useful to run this outside of micropython, in which
    # case you can't reboot using this method :)
    # Candidate for making a signal or some other way to break this out???
    #pylint: disable=import-outside-toplevel
    import machine
    machine.reset()


class Router:
    """ Offsers a publisher-subscriber interface with the ability to restart
    subscribers if they fail to handle a message """
    def __init__(self, topic_list):
        self.topic_list = topic_list

        # Array mapping topics to functions of live instances.
        # When an event needs to be publisheds, the array of callbacks
        # can be retireved from this map
        self._signals = {} # topic, {instance_name: function, instance_name, function}
        for topic in topic_list:
            self._signals[topic] = {}

        # A map of live instances. This is used so that they can be
        # referenced by name for purposes of creation and destruction
        self._instances = {
            "router": self # module_name: instance
        }

        # maps from an instance name to the set of instructions to
        # create/destroy the instance
        self._instance_definitions = {
            "router": InstanceDef(
                reset_system,
                {
                    t.StartInstance: "_start_instance",
                    t.StopInstance: "_stop_instance",
                }
            )
        }

        self.bind_instance("router", self._instances["router"])


        # Signals are executed sequentially. A call to publish inserts the signal
        # into the global signal queue, which is emptied by the end of each
        # call to `distribute_signals`
        # Here we give the deque a finite length and cause it to throw an error
        # if the length is exceeded
        self.current_signals = DEQUE((), 30, 1)

        """ Broadcast a message to any modules subscribed to it's associated
        topic. It will only be broadcast after the current topic is finished
        being broadcast.

        This function may raise an error if there are too many signals awaiting
        transmission. Consider increasing the length of the queue in the
        class definition """
        # Performance improvement:
        self.publish = self.current_signals.append



    def register_instance(self, instance_name, spawn_function, connections):
        """ Register a constructor function to a name and some connections so
        that the router can create/destroy it as needed """
        assert instance_name not in self._instance_definitions
        self._instance_definitions[instance_name] = InstanceDef(spawn_function, connections)


    def bind_instance(self, instance_name, instance):
        """ Ensures that all signals in this instance are connected into the
        signal map """
        assert instance_name in self._instance_definitions
        assert instance in self._instances.values()

        signal_map = self._instance_definitions[instance_name].signals
        for topic, function_name in signal_map.items():
            self._subscribe(topic, instance, instance_name, function_name)


    def _subscribe(self, topic, instance, instance_name, function_name):
        """ Make a function into a callback for a specific topic. When a message
        is published to the topic (using `publish`), this function will be called.

        IF THE CALLBACK FUNCTION FAILS, THE MODULE WILL BE SWAPPED OUT
        """
        assert topic in self.topic_list
        assert instance in self._instances.values()

        function = getattr(instance, function_name)

        self._signals[topic][instance_name] = function


    def unbind_instance(self, instance_name, instance):
        """ Ensures that the functions/signals of an instance are not in the
        signal list. This function will not fail if a function is not in
        the signal list and aside from incorrect instance/instance_names, will
        not fail"""
        assert instance_name in self._instance_definitions
        assert instance in self._instances.values()

        signal_map = self._instance_definitions[instance_name].signals
        for topic, function_name in signal_map.items():
            try:
                self._unsubscribe(topic, instance, instance_name, function_name)
            except Exception as err:
                print("Failed to unsubscribe {} from {}: {}".format(function_name, instance_name, err))


    def _unsubscribe(self, topic, instance, instance_name, function_name):
        """ Removes a function from being an active signal, undoes what
        "subscribe" does. This function will throw an exception if the
        instance/function_name is not subscribed to the topic.
        """
        assert topic in self.topic_list
        assert instance in self._instances.values()

        function = getattr(instance, function_name)

        existing = self._signals[topic]
        del existing[instance_name]


    @micropython.native
    def _publish(self, event):
        """ Broadcast this message to the rest of the system. """

        topic = type(event)  # This takes about 107 - 124 us
        # assert topic in self.topic_list  # This takes about 142us

        for instance_name, callback in list(self._signals[topic].items()):
            try:
                #import time
                #start_time = time.ticks_us()

                callback(event)

                #print(time.ticks_us() - start_time, self.get_instance_from_function(callback), topic)

            except Exception as err:
                print(instance_name, topic)
                if topic != t.MessageMishandled:
                    # Prevent stack overflow if there is an issue in a mishandled handler :p
                    outp = io.StringIO()
                    PRINT_EXCEPTION(err, outp)
                    print(topic, event, outp.getvalue())

                    self.publish(t.MessageMishandled(
                        instance_name, outp.getvalue(), event
                    ))

                if instance_name != "router":
                    print("Restarting")
                    self.publish(t.StopInstance(instance_name))
                    self.publish(t.StartInstance(instance_name))


    def get_instance_from_function(self, function):
        """ returns an instance name from a function belonging to that instance.
        This function is relatively slow and should only be used in error
        handling etc. Returns None if it could not find the function
        """
        return None


    def _start_instance(self, instance):
        """ Starts an instance running """
        assert instance.name in self._instance_definitions

        # Ensure that there isn't already an instance running
        if instance.name in self._instances:
            self._stop_instance(instance)
        assert instance.name not in self._instances

        # Try to create the instance
        try:
            created = self._instance_definitions[instance.name].spawn_function()
        except Exception as err:
            # We couldn't create the instance
            print(err)
            outp = io.StringIO()
            PRINT_EXCEPTION(err, outp)
            self.publish(t.StartInstanceFailed(instance.name, outp.getvalue()))
        else:
            # Try to subscribe the instance to all it's signals
            self._instances[instance.name] = created
            try:
                self.bind_instance(instance.name, created)
            except Exception as err:
                # Connections could not be made.
                print(err)
                outp = io.StringIO()
                PRINT_EXCEPTION(err, outp)
                self.unbind_instance(instance.name, created)
                self.publish(t.StartInstanceFailed(instance.name, outp.getvalue()))


    def _stop_instance(self, instance):
        """ Stops an instance running """
        assert instance.name in self._instance_definitions
        if instance.name not in self._instances:
            return

        self.unbind_instance(instance.name, self._instances[instance.name])
        del self._instances[instance.name]


    @micropython.native
    def update(self):
        while self.current_signals:
            signal = self.current_signals.popleft()  # About 140us on the ESP32
            self._publish(signal)



# Singleton rebinds for convenience
#pylint: disable=invalid-name
_router = None
publish = None
register_instance = None
update = None

def init_router(topic_list):
    """ Initilize the router singleton with the provided topic list. The old
    router will be lost """
    # pylint: disable=global-statement
    global _router
    global publish
    global register_instance
    global update
    _router = Router(topic_list)
    publish = _router.publish
    register_instance = _router.register_instance
    update = _router.update

