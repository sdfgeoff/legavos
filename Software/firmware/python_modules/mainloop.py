""" Something has to call the routers poll event.

This mainloop also tracks the processing duration and uptime and periodically
logs this information
"""
# We use some micropython-specific things, so:
#pylint: disable=no-member
import gc
import time
import router as r
import topics as t
import telemetry
import log


# Used to smooth values reported in the stats message. Higher values = smoother
AVERAGING_CONSTANT = 0.9


class MainLoop:
    """ Responsible for running the main event loop (aka router) and monitoring
    the system (eg cpu usage)  """
    def __init__(self, frequency):
        self.frequency = frequency
        self.loop_microseconds = 1e6 / frequency

        self.prev_processing_start_time = time.ticks_us()
        self.average_processing_duration = 0

        self.missed_loop_counter = 0
        self.total_loop_counter = 0


    def run_single(self):
        """ Runs a single iterations. Returns the number of microseconds until
        it should be run again. """
        processing_start_time = time.ticks_us()
        self.total_loop_counter += 1

        # Figure out how long it has been since the last run and publish a poll
        # event to the router
        time_since_last_update = time.ticks_diff(
            processing_start_time,
            self.prev_processing_start_time
        )
        self.prev_processing_start_time = processing_start_time
        assert time_since_last_update > 0
        r.publish(t.PollUpdate(time_since_last_update * 1e-6))
        r.update()

        # If the garbage gets to big, it could cause a single loop to suddenly
        # stall. The more frequenly you collect it, the shorter it takes.
        # So we may as well collect it each frame so it doesn't cause timing
        # jitter
        gc.collect()

        # Log statistics
        loop_offset = self.total_loop_counter % (self.frequency)
        if loop_offset == 0:
            telemetry.send_debug(telemetry.UPTIME, int(time.time()))

        elif loop_offset == 2:
            cpu_percent = int(self.average_processing_duration / self.loop_microseconds * 100)
            telemetry.send_below_lims(telemetry.CPU_LOAD, cpu_percent, 50, 75)

        elif loop_offset == 4:
            telemetry.send_below_lims(telemetry.MISSED_LOOPS, self.missed_loop_counter, self.frequency / 8, self.frequency / 4)
            self.missed_loop_counter = 0

        elif loop_offset == 6:
            mem = gc.mem_free()
            telemetry.send_above_lims(telemetry.MEMORY_FREE, mem, 50000, 10000)

        processing_stop_time = time.ticks_us()

        # Figure out how long we need to sleep for to achieve the desired
        # loop frequency
        processing_duration = time.ticks_diff(
            processing_stop_time,
            processing_start_time
        )
        sleep_duration = self.loop_microseconds - processing_duration

        self.average_processing_duration = (
            self.average_processing_duration * AVERAGING_CONSTANT + \
            processing_duration * (1.0 - AVERAGING_CONSTANT)
        )

        if sleep_duration < 0:
            self.missed_loop_counter += 1
            return 0
        return int(sleep_duration)


    def log_stats(self):
        """ Push a bunch of stats about the mainloop to the log """




def run(frequency):
    """ Infinite loop the mainloop """
    loop = MainLoop(frequency)
    while True:
        time.sleep_us(loop.run_single())
