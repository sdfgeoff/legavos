""" When running the tests, the path to the main python modules needs to be
included in the pythonpath """
import os
import sys

sys.path.append(os.path.normpath(os.path.join(os.path.abspath(__file__), '../../')))
