""" Topics are the main method with which modules communicate. They are
moved from module to module by the router. The topics should be able to be
serialized to be sent across a network """
import collections
import fix_path # pylint: disable=unused-import
import topics as t




DummyTopic1 = t.topic('dummy1', ['a', 'b'], t.json_packing())
DummyTopic2 = t.topic('dummy2', ['c', 'd'], t.struct_packing("fb"))



def test_json_serialize_deserialize():
    """ Tests that topic are serialized """
    message_1 = DummyTopic1(1, 2)
    serialized = t.serialize(message_1)
    deserialized = t.deserialize(serialized)
    assert deserialized.a == 1
    assert deserialized.b == 2


def test_struct_serialize_deserialize():
    """ Tests that topic are serialized """
    message_1 = DummyTopic2(4.5, 2)
    serialized = t.serialize(message_1)
    deserialized = t.deserialize(serialized)
    assert deserialized.c == 4.5
    assert deserialized.d == 2
