""" The router is the core of the robot, and is responsible for allowing
software modules to communicate """
import collections
import weakref
import fix_path # pylint: disable=unused-import
import router as r
import topics as t


DummyTopic1 = collections.namedtuple('dummy1', ['a', 'b'])
DummyTopic2 = collections.namedtuple('dummy2', ['c', 'd'])

TOPIC_LIST = [
    t.PollUpdate,
    t.StartInstance,
    t.StopInstance,
    t.MessageMishandled,
    t.StartInstanceFailed,
    DummyTopic1,
    DummyTopic2,
]



class FailingModule:
    """ A module that fails after n calls """
    def __init__(self, fail_after_n_calls):
        self.count_remaining = fail_after_n_calls

    def execute(self, _event):
        """ Run the failing module. This will decrement the number of
        calls until the module fails, and when this reaches zero, the module
        will raise an assertion error """
        if self.count_remaining == 0:
            assert False

        self.count_remaining -= 1




def test_constructs_and_discards_instances():
    """ An instance is registered to the router, and when the router
    is instructed to start the instance with a StartInstance message, the
    constructor function is called.
    Similarly, when the router is instructed to destroy the object with
    a "StopInstance" message, check that references to the object are
    removed so it can be/is garbage collected """
    # Every time an object is constructed store a weak reference to it.
    # This means we can check if objects are deleted by checking if the
    # weakref still contains an object
    constructor_calls = []
    def constructor():
        obj = FailingModule(-1)
        constructor_calls.append(weakref.ref(obj))
        return obj

    router = r.Router(TOPIC_LIST)
    router.register_instance(
        "test_instance",
        constructor,
        {}
    )

    # Constructor should not be called when registering an instance
    assert len(constructor_calls) == 0

    # Nor should it automatically start
    router.update()
    router.update()
    router.update()
    router.update()
    assert len(constructor_calls) == 0

    # But it should start when requested
    router.publish(t.StartInstance("test_instance"))
    router.update()
    assert len(constructor_calls) == 1
    assert constructor_calls[0]() is not None

    # Should exist for a while
    router.update()
    router.update()
    router.update()
    router.update()

    # And lets delete it
    router.publish(t.StopInstance("test_instance"))
    router.update()
    assert len(constructor_calls) == 1
    assert constructor_calls[0]() is None


def test_subscription():
    """ The point of the router is that an emitted event causes a callback
    on another object to be called. """
    router = r.Router(TOPIC_LIST)

    # Register a lists's append function as an instance so we can monitor what
    # events get sent to it
    call_list = list()
    router.register_instance(
        "test_instance",
        lambda: call_list,
        {DummyTopic1:"append"}
    )
    router.publish(t.StartInstance("test_instance"))
    router.update()

    message1 = DummyTopic1(1, 2)
    message2 = DummyTopic1(3, 4)

    assert len(call_list) == 0

    router.publish(message1)
    router.update()
    assert call_list[0] is message1

    router.publish(message1)
    router.publish(message2)
    router.update()
    assert call_list[1] is message1
    assert call_list[2] is message2
    assert len(call_list) == 3

    # Things happening on other topics shouldn't affect this object
    router.publish(DummyTopic2(1, 2))
    assert len(call_list) == 3


def test_emit_failed_start_event():
    """ If an instance fails to start, the router should send a
    StartInstanceFailed message so that other things can be notified about the
    failure. """
    # Our construction function will fail every time!
    def constructor():
        assert False

    router = r.Router(TOPIC_LIST)
    router.register_instance(
        "failing_instance",
        constructor,
        {}
    )

    # To track the StartInstanceFailed, we'll subscribe a lists append function
    # to catch the event
    call_list = list()
    router.register_instance(
        "test_instance",
        lambda: call_list,  # Our constructor returns the existing list
        {t.StartInstanceFailed:"append"}
    )
    router.publish(t.StartInstance("test_instance"))
    router.publish(t.StartInstance("failing_instance"))
    router.update()

    assert call_list[0].instance_name == "failing_instance"
    assert call_list[0].traceback != ""
    assert len(call_list) == 1



def test_execption_results_in_message_mishandled():
    """ When an instance throws an error, the router needs to detect this
    event and generate a message about it """
    router = r.Router(TOPIC_LIST)
    router.register_instance(
        "failing_instance",
        lambda: FailingModule(1),
        {DummyTopic1:"execute"}
    )

    call_list = list()
    router.register_instance(
        "test_instance",
        lambda: call_list,
        {t.MessageMishandled:"append"}
    )
    router.publish(t.StartInstance("test_instance"))
    router.publish(t.StartInstance("failing_instance"))
    router.update()
    assert len(call_list) == 0  # The instance did start

    message1 = DummyTopic1(1, 2)
    router.publish(message1)
    router.update()
    assert len(call_list) == 0  # The first call did not throw an exception

    message1 = DummyTopic1(1, 2)
    router.publish(message1)
    router.update()

    # The second call did throw an exception, so a message should have been
    # emitted
    assert call_list[0].instance_name == "failing_instance"
    assert call_list[0].traceback != ""
    assert len(call_list) == 1



def test_exception_results_in_instance_restart():
    """ When a module fails to handle a message, it's probably unintentional
    so the router should restart the module """
    # To count how many times a local function runs. A list is used because
    # it is passed by reference (ints are not)
    start_count = []

    def create():
        start_count.append(0)
        return FailingModule(1)  # fail the second time a message arrives


    router = r.Router(TOPIC_LIST)
    router.register_instance(
        "failing_instance",
        create,
        {DummyTopic1:"execute"}
    )

    # Constructor should not have been called before explicitly asked
    assert len(start_count) == 0

    # Start the instance and check that it started
    router.publish(t.StartInstance("failing_instance"))
    router.update()
    assert len(start_count) == 1

    # The instance doesn't fail on the first call, so make sure it isn't
    # restarted accidentally
    message1 = DummyTopic1(1, 2)
    router.publish(message1)
    router.update()
    assert len(start_count) == 1

    # However it will fail on the second call, so the constructor should have
    # been called again
    message1 = DummyTopic1(1, 2)
    router.publish(message1)
    router.update()
    assert len(start_count) == 2

    router.publish(message1)
    router.update()
    router.publish(message1)
    router.update()
    assert len(start_count) == 3
