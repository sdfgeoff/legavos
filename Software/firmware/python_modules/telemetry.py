import topics as t
import router as r
import log

CPU_LOAD = 0
MEMORY_FREE = 1
MISSED_LOOPS = 2
UPTIME = 3
GAIT_MACHINE = 4
GUN = 5

NAME_LOOKUP = {v:k for k, v in globals().items() if isinstance(v, int)}


FORMATTERS = {
    CPU_LOAD: "{}%".format,
    MEMORY_FREE: lambda x: "{}kb".format(int(x / 100)),
    MISSED_LOOPS: "{}".format,
    UPTIME: lambda x: "{:02}:{:02}".format(int(x / 60 + 0.5), x % 60),
    GAIT_MACHINE: lambda x: ("init", "idle", "active")[x],
    GUN: lambda x: ("inactive", "armed", "loading", "unloading")[x],
}


def send_below_lims(channel, value, warn_level, error_level):
    """ Send a telemetry message for the specified value, automatically filling
    in the level. If teh value is lower than the warn_level it will be sent
    as "DEBUG" level. If it is below error_level it will be sent as "WARN". Any
    value greater than the error_level will be sent as "ERROR"
    """
    assert channel in NAME_LOOKUP
    assert error_level > warn_level
    if value < warn_level:
        level = log.DEBUG
    elif value < error_level:
        level = log.WARNING
    else:
        level = log.ERROR

    r.publish(t.Telemetry(level, channel, value))


def send_above_lims(channel, value, warn_level, error_level):
    """ Send a telemetry message for the specified value, automatically
    filling in the level. If the value is greater than the warn_level it will
    be sent as "DEBUG" level. If it is between error and warn it will be sent
    as "WARN". Any value lower than the error_level will be sent as "ERROR"
    """
    assert channel in NAME_LOOKUP
    assert error_level < warn_level
    if value > warn_level:
        level = log.DEBUG
    elif value > error_level:
        level = log.WARNING
    else:
        level = log.ERROR

    r.publish(t.Telemetry(level, channel, value))


def send_debug(channel, value):
    r.publish(t.Telemetry(log.DEBUG, channel, value))
