""" This module is imported automatically by micropython when the robot starts.
Thus it is responsible for setting up and running the robot!
"""
import sys

import router as r
import topics as t

import log
import mainloop
import version

from modules.log_printer import LogPrinter
from modules.router_log_bridge import RouterLogBridge
from modules.servo import Servo, MG996R, SG90
from modules.wifi_network_join import JoinWifiNetwork
from modules.wifi_network_host import HostWifiNetwork
from modules.udp_heartbeat import HeartbeatUDP
from modules.udp_bridge import UdpBridgeSender
from modules.udp_bridge import UdpBridgeListener
from modules.gait_machine import GaitMachine
from modules.turret import Turret
from modules.dynamixel_bus import DynamixelBus
from modules.dynamixel_servo import DynamixelServo
from modules.gun import Gun

if sys.platform == "linux":
    import dummy_machine as machine
else:
    import machine

if sys.implementation.name != "micropython":
    print(
        "This is designed for micropython and may not run properly in cpython."
        " It should be invoked on a local machine using `make test`"
    )


def setup_modules():
    """ Tell the router what modules the robot can run """

    # ------------------- Logging/system services------------------------------
    r.register_instance("log_printer", LogPrinter, {
        t.Log: "print_log",
        t.Telemetry: "print_telemetry",
    })
    r.register_instance("router_log_bridge", RouterLogBridge, {
        t.StopInstance: "stop_instance",
        t.StartInstance: "start_instance",
        t.MessageMishandled: "message_mishandled",
        t.StartInstanceFailed: "start_instance_failed",
    })

    # ------------------------ Hardware/servos --------------------------------
    r.register_instance(
        "leg_select_servo",
        lambda: Servo(machine.PWM(machine.Pin(2)), MG996R, 110, 1),
        {t.CommandLegSelectServo: "set_angle"}
    )
    r.register_instance(
        "leg_left_servo",
        lambda: Servo(machine.PWM(machine.Pin(13)), MG996R, 98, -1),
        {t.CommandLegLeftServo: "set_angle"}
    )
    r.register_instance(
        "leg_right_servo",
        lambda: Servo(machine.PWM(machine.Pin(4)), MG996R, 94, 1),
        {t.CommandLegRightServo: "set_angle"}
    )
    r.register_instance(
        "turret_pan_servo",
        lambda: Servo(machine.PWM(machine.Pin(15)), SG90, 100, -1),
        {t.CommandTurretPanServo: "set_angle"}
    )

    r.register_instance(
        "turret_tilt_servo",
        lambda: DynamixelServo(0x54, 130),
        {t.CommandTurretTiltServo: "set_angle"}
    )
    
    r.register_instance(
        "dynamixel_bus",
        lambda: DynamixelBus(machine.UART(1), 12, 14, 57600),
        {t.DynamixelMessage: "transmit"}
    )


    # --------------------- Communication systems -----------------------------
    r.register_instance(
        "wifi_network",
        lambda: HostWifiNetwork("legavos", "legavos"),
        {}
    )
    r.register_instance(
        "udp_heartbeat",
        lambda: HeartbeatUDP("legavos_was_here", 5.0, 37020),
        {t.PollUpdate: "update"}
    )

    r.register_instance(
        "telemetry_transmitter",
        lambda: UdpBridgeSender("192.168.4.2", 37021),
        {
            t.Log: "transmit",
            t.Telemetry: "transmit",
        }
    )

    r.register_instance("control_receiver", lambda: UdpBridgeListener(37022), {
        t.PollUpdate: "update"
    })

    r.register_instance("gait_machine", GaitMachine, {
        t.PollUpdate: "update",
        t.ControlPacket: "set_control"
    })
    
    r.register_instance("gun_controller", lambda: Gun(0x47), {
        t.ControlPacket: "set_control_packet"
    })

    r.register_instance("turret_controller", Turret, {
        t.ControlPacket: "set_control",
        t.WalkCycleBodyAngle: "set_walk_cycle_angle",
    })


def start():
    """ Start the robot running """
    r.init_router(t.TOPIC_LIST)

    setup_modules()
    r.publish(t.StartInstance("router_log_bridge"))
    r.publish(t.StartInstance("log_printer"))

    log.info(
        "boot_info",
        {
            "python":sys.implementation.name,
            "platform": sys.platform,
            "hash": version.GIT_HASH
        }
    )

    r.publish(t.StartInstance("udp_heartbeat"))
    r.publish(t.StartInstance("telemetry_transmitter"))

    r.publish(t.StartInstance("leg_select_servo"))
    r.publish(t.StartInstance("leg_left_servo"))
    r.publish(t.StartInstance("leg_right_servo"))
    r.publish(t.StartInstance("turret_pan_servo"))
    r.publish(t.StartInstance("turret_tilt_servo"))

    if sys.platform == "esp32":
        # Wehn wer're running on a linux computer, we don't have access
        # to any of the actual hardware, so we only start it if
        r.publish(t.StartInstance("wifi_network"))
        r.publish(t.StartInstance("dynamixel_bus"))

    r.publish(t.StartInstance("control_receiver"))
    r.publish(t.StartInstance("gait_machine"))
    r.publish(t.StartInstance("turret_controller"))
    r.publish(t.StartInstance("gun_controller"))
    


def run():
    mainloop.run(50)


start()
if __name__ == "__main__":
    run()
