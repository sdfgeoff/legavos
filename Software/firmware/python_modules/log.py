""" Logging is important(tm). Log levels are taken from syslog


This file is just a bunch of convenience functions
that allow you to write:
    "log.info("message", {"some":"data"})
rather than
    router.publish(router.log(6, message, {"some":"data"})

This also means that logging can be subbed out for other systems in the future
"""

import router as r
import topics as t


# Log Levels
EMERGENCY = 0
ALERT = 1
CRITICAL = 2
ERROR = 3
WARNING = 4
NOTICE = 5
INFO = 6
DEBUG = 7


def _send(level, message, fields):
    """ Forward the log message on to the event router """
    r.publish(t.Log(level, message, fields))


def emergency(message, fields):
    """ System is unusable """
    _send(EMERGENCY, message, fields)


def alert(message, fields):
    """ Action must be taken immediately """
    _send(ALERT, message, fields)


def critical(message, fields):
    """ Hard device errors """
    _send(CRITICAL, message, fields)


def error(message, fields):
    """ Error conditions """
    _send(ERROR, message, fields)


def warning(message, fields):
    """ Warning conditions """
    _send(WARNING, message, fields)


def notice(message, fields):
    """ Conditions that are not error conditions, but that may require special handling. """
    _send(NOTICE, message, fields)


def info(message, fields):
    """ Informational messages """
    _send(INFO, message, fields)


def debug(message, fields):
    """ Messages that contain information normally of use only when debugging a program """
    _send(DEBUG, message, fields)
