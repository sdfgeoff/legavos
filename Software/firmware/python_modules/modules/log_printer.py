""" While debugging, log information should go via UART (if testing on the
controller) or to stdout (if running from a terminal). The log printer
should be subscribed to the LOG event """
import telemetry

class LogPrinter:
    """ Prints log messages to stdout in tabular form """
    LEVEL_NAMES = {
        0: "emerg",
        1: "alert",
        2: "crit",
        3: "err",
        4: "warning",
        5: "notice",
        6: "info",
        7: "debug",
    }

    def print_log(self, message):
        """ Format a message and print it """
        level_name = self.LEVEL_NAMES[message.level]
        print("{:8} | {:20} | {}".format(level_name, message.message, message.fields))

    def print_telemetry(self, telem):
        """ Format and print a telemetry message """
        level_name = self.LEVEL_NAMES[telem.level]
        channel_name = telemetry.NAME_LOOKUP[telem.key]
        print("{:8} | {:20} | {}".format(level_name, channel_name, telem.value))
