""" Controls the gun """
import router as r
import topics as t
import telemetry

class Gun:
    def __init__(self, dynamixel_address):
        self.address = dynamixel_address
        self.previous_shot_id = 0

        self.mode = t.GUN_MODE_INACTIVE


    def set_control_packet(self, control_packet):
        if self.mode != control_packet.gun_mode:
            self._set_mode(control_packet.gun_mode)

        if self.mode == t.GUN_MODE_ARMED:
            if self.previous_shot_id != control_packet.gun_bullet_id:
                self.previous_shot_id = control_packet.gun_bullet_id
                self._shoot()
        else:
            self.previous_shot_id = control_packet.gun_bullet_id

    def _shoot(self):
        r.publish(
            t.DynamixelMessage(
                self.address,
                0x03,
                bytearray([
                    31, 0, # Most servos have the goal position in position 30
                    1 # Set the number of shots to "1"
                ])
            )
        )

    def _set_mode(self, mode):
        self.mode = mode
        telemetry.send_debug(telemetry.GUN, mode)
        r.publish(
            t.DynamixelMessage(
                self.address,
                0x03,
                bytearray([
                    30, 0, # Most servos have the goal position in position 30
                    mode
                ])
            )
        )
