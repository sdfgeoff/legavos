""" For developing some of the higher-level functionality, having to re-flash
the microcontroller can be cumbersome. The UDPBridge system allows separating
parts of the robot over a network. The UdpBridgeListener will emit any events
that were sent to it via the UDPServo Sender.

eg:

Local Routing System ------ LegServoCommandEvent -----> UDPBridgeSender
                                                               |
      _________________________________________________________|
     |
     V
UDPBridgeListener ------ LegServoCommandEvent(re-emit of above) -> Robot Routing system

A bridge can be subscribed to multiple event types

-----------------------------------------------------------------

The UDPBridge only works if there haven't been any changes to the topics.py
file. There is some rudimentary change detection which causes received messages
to be discarded, but this may not be 100% reliable.

-----------------------------------------------------------------

This is not designed for any security, and no validation of sender is performed.
Running this bridge may allow other people control over the robots systems.

-----------------------------------------------------------------

To avoid conflicts, you may need to make sure that other modules that would
normally transmit onto the same topics are disabled.
"""
import sys
import socket
import topics as t
import router as r



class UdpBridgeListener:
    """ Listens on the specified port for incomming UDP messages. It then
    attempts to deserialize them """
    def __init__(self, port):
        self.port = port

        if sys.platform == "esp32":
            # The ESP32 has the full socket implementation (the ESP has the
            # lwip stack), but the unix port has only usocket which has some quirks.
            self.client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)

        else:
            self.client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        self.client.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.client.bind(socket.getaddrinfo("0.0.0.0", port)[0][-1])
        self.client.settimeout(0.0)

    def update(self, _message):
        """ Check if there is any data on the port and try parse it as a
        message"""
        i = 0
        while i < 10:
            try:
                i += 1
                data, _addr = self.client.recvfrom(1024)
            except OSError:
                break
            else:
                message = t.deserialize(data)
                if message is not None:
                    r.publish(message)





class UdpBridgeSender:
    """ Whenever an event received by the "Transmit" function it is
    serialized and sent to the port and address specified in the initilizer """
    def __init__(self, ip_address, port):

        if sys.platform == "esp32":
            # The ESP32 has the full socket implementation (the ESP has the
            # lwip stack), but the unix port has only usocket which has some quirks.
            self.server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
            self.address = (ip_address, port)
        else:
            self.server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.address = socket.getaddrinfo(ip_address, port)[0][-1]

        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.settimeout(0.0)

    def transmit(self, message):
        """ Serialize and send this event """
        data = t.serialize(message)
        try:
            self.server.sendto(
                data,
                self.address
            )
        except OSError as err:
            print("Failed to send message")
            if err.args[0] != 118:
                raise err
