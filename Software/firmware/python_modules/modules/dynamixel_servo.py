""" Allows control of dynamixel smart servos using a simplified interface """
import topics as t
import router as r


class DynamixelServo:
    """ Provides an interface similar to the "Servo" class, but
    redirects the command to the Dynamixel Bus for actual transmission """
    def __init__(self, address, zero_angle):
        self.address = address
        self.zero_angle = zero_angle

    def set_angle(self, angle):
        """ Set the angle of this servo """
        # Each "tick" is 0.29 of a degree for many dynamixel servos
        angle_int = int((angle.degrees + self.zero_angle) / 0.29)
        assert angle_int > 0
        assert angle_int < 1024
        r.publish(
            t.DynamixelMessage(
                self.address,
                0x03,
                bytearray([
                    30, 0, # Most servos have the goal position in position 30

                    angle_int % 256, angle_int >> 8
                ])
            )
        )
