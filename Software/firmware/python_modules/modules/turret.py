""" Controls the motion (pan, tilt) of the turret system """

import router as r
import topics as t

class Turret:
    """ The turret holds the electronics, camera and gun. It should point
    in the direction of motion and it should be able to be pointed from side
    to side by the user. For this to happen, the turret needs to be aware of
    the offset between it's mounting point and the current part of the walk
    cycle as well as input as to where the user is pointing it """
    def __init__(self):
        self.walk_cycle_angle = 0
        self.elevation = 0
        self.pan = 0

        self.current_pan_angle = 0

    def set_walk_cycle_angle(self, angle):
        """ As the robot walks, the chassis rotates from side to side.
        This function allows the robot to offset for it """
        self.walk_cycle_angle = angle.degrees
        self.compute()

    def set_control(self, control_packet):
        """ The user wants to be able to point the turrets at things. This
        function expects a CommandTurret message """
        self.elevation = control_packet.turret_elevation / 10
        self.pan = control_packet.turret_pan / 10
        self.compute()

    def compute(self):
        """ Because there are multiple sources that will change the output,
        the computation of the output is split into this function. It is
        responsible for combining the walk cycle and commanded angles
        and outputting to the rest of the system """
        output_pan_angle = self.pan + self.walk_cycle_angle
        r.publish(t.CommandTurretPanServo(int(output_pan_angle)))
        r.publish(t.CommandTurretTiltServo(int(self.elevation)))
