""" Use the networking hardware on the ESP series chips to connect to
an existing wifi network """


class HostWifiNetwork:
    """ Host a wifi network from the ESP """
    def __init__(self, ssid, password):
        import network
        print("Hosting Network {}".format(ssid))
        self.ssid = ssid
        self.password = password

        self.access_point = network.WLAN(network.AP_IF)
        self.access_point.config(essid=ssid)
        self.access_point.config(password=password)
        self.access_point.config(max_clients=10)

        self.access_point.active(True)
