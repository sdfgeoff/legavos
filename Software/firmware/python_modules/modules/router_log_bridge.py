""" The router can't log directly, so this module logs events that occur
inside the router """
import log


class RouterLogBridge:
    """ Ensures that important events that the router generates find their
    way into the log files. This is an external file to avoid a circular
    dependency between the router and log system (the log system uses the
    router internally) """

    @staticmethod
    def start_instance(instance):
        """ The router has started an instance """
        log.notice("starting_instance", {"instance": instance.name})

    @staticmethod
    def stop_instance(instance):
        """ The router has stopped an instance """
        log.notice("stopping_instance", {"instance": instance.name})

    @staticmethod
    def message_mishandled(details):
        """ An instance raised an exception while handling a message """
        log.error(
            "message_mishandled",
            {
                "instance":details.instance_name,
                "traceback": details.traceback,
                "message": details.message
            }
        )

    @staticmethod
    def start_instance_failed(details):
        """ The router tried to start an instance, but the instance's
        constructor raised an exception """
        log.emergency(
            "start_instance_failed",
            {"instance":details.instance_name, "traceback": details.traceback}
        )
