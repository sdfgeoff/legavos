""" When connected on a normal network, identifying the IP address of the
robot can br frustrating. This module posts a message over UDP broadcast every
few seconds. This can be picked up by another system and used to locate the
robot """
import socket
import sys


class HeartbeatUDP:
    """ Broadcast a message repeatedly on UDP broadcast channels. This
        allows the device to be discovered. Parameters:
         - message: the message to broadcast
         - period: how often (in seconds) to broadcast the message
         - port: what UDP port to broadcast on
    """
    def __init__(self, message, period, port):
        self.time_since_broadcast = 0
        self.time_between_broadcasts = period

        if sys.platform == "esp32":
            # The ESP32 has the full socket implementation (the ESP has the
            # lwip stack), but the unix port has only usocket which has some quirks.
            self.server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
            self.address = ("255.255.255.255", port)
        else:
            self.server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.address = socket.getaddrinfo("255.255.255.255", port)[0][-1]
            # Linux generates a permissions error if you try broadcast without
            # letting it know
            self.server.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)


        self.message = message.encode('utf-8')
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.settimeout(0.0)

        # We don't call broadcast_now here because if it fails then the
        # service will no longer be run. Sending can fail if there is no network etc.


    def update(self, event):
        """ Update the internal timer and broadcast if the time has bee
        exceeded """
        self.time_since_broadcast += event.deltatime
        if self.time_since_broadcast > self.time_between_broadcasts:
            self.broadcast_now()


    def broadcast_now(self):
        """ Send the message out into the world """
        self.time_since_broadcast = 0
        self.server.sendto(self.message, self.address)
