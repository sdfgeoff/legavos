""" Generates a motion of servos to move the robot in a particular direction """

import math
import micropython

import router as r
import topics as t
import log
import telemetry


# The angle difference from the neutral angle that the pickup servo will rotate
# through when picking up a leg
LEG_SELECT_PICKUP_ANGLE = 45

# THe angles that the leg select servo is in when picking up teh various legs
LEG_SELECT_LEFT_RAISED = LEG_SELECT_PICKUP_ANGLE
LEG_SELECT_RIGHT_RAISED = - LEG_SELECT_PICKUP_ANGLE


LEG_SERVO_SWING_ANGLE = 22

# If you don't set the velocity for this period of time, the walk generation
# will stop
NO_COMMAND_TIMEOUT = 1.0

# How short a single step can be when the robot is moving full speed
GAIT_CYCLE_TIME_MINIMUM = 2.0

# How long a single step can be when the robot is voging at low speeds
GAIT_CYCLE_TIME_MAXIMUM = 2.0


class GaitMachine:
    """ The gait machine is responsible for moving the servos to allow the robot
    to move. However, it has to be safe: If no command is received
    for more than a few seconds, the robot should stop moving. """
    INIT = 0  # The mode used to initilize the servos one at a time
    IDLE = 1  # The mode used to "idle" - when there has been no user input
    ACTIVE = 2  # The mode used when generating the robot's normal gait

    def __init__(self):
        self.time = 0
        self.current_gait_percent = 0  # Percentage through the current step

        self.linear_speed = 0.0
        self.angular_speed = 0.0
        self.set_motion_time = 0  # Time since the gait generator was told what to do

        self._mode = self.INIT

    @property
    def mode(self):
        """ Sets what type of gait the gait machine is currently generating."""
        return self._mode

    @mode.setter
    def mode(self, val):
        telemetry.send_debug(telemetry.GAIT_MACHINE, val)
        log.info("gait_machine_set_mode", {"old": self._mode, "new": val})
        assert val != self._mode  # Nothing should no-op change intentionally

        # Reset the shared variables
        self.set_motion_time = 0
        self.time = 0
        self.current_gait_percent = 0

        # Finally set the new mode
        self._mode = val



    def set_control(self, control_packet):
        """ Sets the direciton/velocity that the gait machine will try and
        get the robot to walk in """
        self.set_motion_time = self.time

        self.linear_speed = clamp(control_packet.chassis_forwards/100, -1, 1)
        self.angular_speed = clamp(control_packet.chassis_rotate/100, -1, 1)

        if self.mode == self.IDLE:
            self.mode = self.ACTIVE


    def run_idle(self):
        """ Return legs to neutral """
        self.command_servos(0, 0, 0, 0)


    @micropython.native
    def run_active(self, deltatime):
        """ This is the walk gait as controlled by the `set_motion` function """
        if self.time > self.set_motion_time + NO_COMMAND_TIMEOUT:
            self.mode = self.IDLE
            return

        # If the robot is asked to move fast, it should move it's legs fast
        # if it's asked to move slow, use more gentle motions
        total_speed = abs(self.linear_speed) + abs(self.angular_speed)
        total_speed = clamp(total_speed, 0, 1)
        if total_speed < 0.05 and self.current_gait_percent < 0.05:
            gait_cycle_time = 0.0
            return
        gait_cycle_time = lerp(GAIT_CYCLE_TIME_MAXIMUM, GAIT_CYCLE_TIME_MINIMUM, total_speed)


        self.current_gait_percent += deltatime / gait_cycle_time
        if self.current_gait_percent > 1:
            self.current_gait_percent -= 1


        leg_select = math.sin(self.current_gait_percent * 2 * 3.14)
        leg_select *= LEG_SELECT_PICKUP_ANGLE

        # TODO: a sin wave is probably not optimal. Probably a triangle wave
        # is better - higher acceleration load on the picked up servo, but
        # keeps the momentum of the chassis motion
        swing = math.cos(self.current_gait_percent * 2 * 3.14)

        # How much the body has swung by
        body_angle = swing * (-self.linear_speed + self.angular_speed) * LEG_SERVO_SWING_ANGLE

        # How much to swing each leg
        left_swing_angle = swing * (-self.linear_speed - self.angular_speed)
        right_swing_angle = swing * (-self.linear_speed + self.angular_speed)

        left_swing_angle = clamp(left_swing_angle, -1.0, 1.0) * LEG_SERVO_SWING_ANGLE
        right_swing_angle = clamp(right_swing_angle, -1.0, 1.0) * LEG_SERVO_SWING_ANGLE

        self.command_servos(left_swing_angle, right_swing_angle, leg_select, body_angle)



    def update(self, message):
        """ Increment the walk cycle """
        self.time += message.deltatime

        if self.mode == self.ACTIVE:
            self.run_active(message.deltatime)
        elif self.mode == self.IDLE:
            self.run_idle()
        elif self.mode == self.INIT:
            self.run_init()
        else:
            raise Exception("InvalidMode")


    @staticmethod
    def command_servos(left, right, select, body_angle):
        """ Send the commands to the servos"""
        assert abs(left) <= LEG_SERVO_SWING_ANGLE
        assert abs(right) <= LEG_SERVO_SWING_ANGLE
        assert abs(select) <= LEG_SELECT_PICKUP_ANGLE

        #print(int(left), int(right), int(select))
        r.publish(t.CommandLegLeftServo(int(left)))
        r.publish(t.CommandLegRightServo(int(right)))
        r.publish(t.CommandLegSelectServo(int(select)))
        r.publish(t.WalkCycleBodyAngle(body_angle))



    def run_init(self):
        """ This gait turns on the servos one at a time and moves them to
        their neutral position """
        if self.time < 0.25:
            # Center select servo
            r.publish(t.CommandLegSelectServo(0))

        elif self.time < 0.5:
            # Pick up left leg
            r.publish(t.CommandLegSelectServo(LEG_SELECT_LEFT_RAISED))

        elif self.time < 0.75:
            # Center left leg
            r.publish(t.CommandLegSelectServo(LEG_SELECT_LEFT_RAISED))
            r.publish(t.CommandLegLeftServo(0))

        elif self.time < 1.0:
            # Pick up right leg
            r.publish(t.CommandLegSelectServo(LEG_SELECT_RIGHT_RAISED))
            r.publish(t.CommandLegLeftServo(0))

        elif self.time < 1.25:
            # Center right leg
            r.publish(t.CommandLegSelectServo(LEG_SELECT_RIGHT_RAISED))
            r.publish(t.CommandLegRightServo(0))

        elif self.time < 1.5:
            # All legs centered, center the turret
            self.command_servos(0, 0, 0, 0)
        else:
            self.mode = self.IDLE



@micropython.native
def clamp(num: float, min_val: float, max_val: float) -> float:
    """ Ensures a number is between two bounds """
    return max(min(num, max_val), min_val)

@micropython.native
def lerp(from_value: float, to_value: float, percent: float) -> float:
    """ Moves t percent of the way from a to b """
    return to_value * percent + from_value * (1.0 - percent)
