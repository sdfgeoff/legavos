""" Control a normal RC servo via the event router. A servo
is constructed with a ServoPWM specification, which defines the
details needed to set up the hardware for a specific type of servo (ie the
relationship between angle and PWM) """
from collections import namedtuple

# The ServoPWM object contains enough data to set up a PWM device to control
# a servo.
ServoPWM = namedtuple("ServoPWM", ["freq", "ticks_per_degree", "ticks_at_min", "ticks_at_max"])


def create_servo_pwm(freq, min_us, max_us, angle_range, pwm_divisions):
    """ Creates a ServoPWM object for a specific servo and PWM device.
    Input parameters:
     - freq - the frequency you want the servo to be run at. For most servos
              this is 50Hz, but for modern digital servos this can be 100Hz
              allowing higher update rates etc.
     - min_us - microseconds pulse duration for one end of the servos travel
     - max_us - microseconds pulse durection at the other end of the servos
                travel (must be a larger number than min_us)
     - angle_range - The degrees that the servo will move through across the
                     min_is and max_us range
     - pwm_divisions - How many divisions the PWM device offers. On micropython
                       ESP32 this is 1024 regardless of frequency.
    """
    microseconds_per_period = 1_000_000 / freq
    degrees_per_microsecond = angle_range / (max_us - min_us)

    # A tick is a division per period
    ticks_per_microsecond = pwm_divisions / microseconds_per_period

    ticks_per_degree = ticks_per_microsecond / degrees_per_microsecond
    ticks_at_min = ticks_per_microsecond * min_us
    ticks_at_max = ticks_per_microsecond * max_us

    return ServoPWM(
        freq,
        ticks_per_degree,
        ticks_at_min,
        ticks_at_max
    )




MG996R = create_servo_pwm(50, 500, 2300, 180, 1024)
SG90 = create_servo_pwm(50, 500, 2500, 180, 1024)


def angle_to_pwm(servo_spec, degrees):
    """ Computes the output to the PWM device needed to achieve a particular
    angle on a servo """
    return int(servo_spec.ticks_at_min + degrees * servo_spec.ticks_per_degree)



class Servo:
    """ Control a hobby servo motor. All angles are in degrees. The
    center_angle offsets the "set_angle" command so that setting the angle to
    zero can (For example) be the midpoint of the servos travel. Similarly
    angle_scale can be used to adjust the scale of the set_angle command.
    Mostly this is intended to be set to either 1 or -1 (inverted). """
    def __init__(self, pwm_device, servo_pwm_spec, center_angle, angle_scale):
        self.servo_pwm_spec = servo_pwm_spec
        self.center_angle = center_angle
        self.angle_scale = angle_scale
        self._pwm = pwm_device

        self._pwm.freq(servo_pwm_spec.freq)
        self._pwm.duty(0)


    def apply_calibration(self, angle):
        """ Offsets an angle by this servos calibration """
        actual_angle = self.center_angle + angle * self.angle_scale
        return actual_angle

    def set_angle(self, angle):
        """ Sets the angle (in degrees) that the servo should move to """
        self._pwm.duty(angle_to_pwm(self.servo_pwm_spec, self.apply_calibration(angle.degrees)))

    def idle(self):
        """ Stop outputting an angle to the servo. In most cases the servo
        will failsafe by disabling the motor - allowing it to be moved by hand
        """
        self._pwm.duty(0)
