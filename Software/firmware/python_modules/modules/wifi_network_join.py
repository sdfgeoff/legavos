""" Use the networking hardware on the ESP series chips to connect to
an existing wifi network """


class JoinWifiNetwork:
    """ Use the networking hardware on the ESP series chips to connect to
    an existing wifi network """
    def __init__(self, ssid, password):
        import network
        print("Joining Network {}".format(ssid))
        self.ssid = ssid
        self.password = password

        self.station = network.WLAN(network.STA_IF)
        self.station.active(True)
        self.station.connect(self.ssid, self.password)
