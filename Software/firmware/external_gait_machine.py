""" Connects to a remote robot, stops it's gait machine and starts transmitting
gait signals from here """

import os
import sys
sys.path.append(os.path.normpath(os.path.join(os.path.abspath(__file__), "../python_modules")))

import router as r
import topics as t
import time

r.init_router(t.TOPIC_LIST)


from modules.log_printer import LogPrinter
from modules.router_log_bridge import RouterLogBridge
from modules.udp_bridge import UdpBridgeSender
from modules.udp_bridge import UdpBridgeListener
from modules.gait_machine import GaitMachine



def start():

    r.register_instance("log_printer", LogPrinter, {
        t.Log: "print_log",
        t.Telemetry: "print_telemetry",
    })
    r.register_instance("router_log_bridge", RouterLogBridge, {
        t.StopInstance: "stop_instance",
        t.StartInstance: "start_instance",
        t.MessageMishandled: "message_mishandled",
        t.StartInstanceFailed: "start_instance_failed",
    })


    r.register_instance(
        "transmitter",
        lambda: UdpBridgeSender("192.168.4.1", 37022),
        {
            t.CommandLegSelectServo: "transmit",
            t.CommandLegRightServo: "transmit",
            t.CommandLegLeftServo: "transmit",
            t.WalkCycleBodyAngle: "transmit",
        }
    )

    r.register_instance("control_receiver", lambda: UdpBridgeListener(37022), {
        t.PollUpdate: "update"
    })

    r.register_instance("gait_machine", GaitMachine, {
        t.PollUpdate: "update",
        t.ControlPacket: "set_control"
    })

    r.publish(t.StartInstance("log_printer"))
    r.publish(t.StartInstance("router_log_bridge"))
    r.publish(t.StartInstance("transmitter"))
    r.publish(t.StartInstance("control_receiver"))
    r.publish(t.StartInstance("gait_machine"))

    # Ensure the modulles are started
    r.publish(t.PollUpdate(0.016))
    r.update()


    # Hacky stuff to transmit a Stop signal
    r._router._instances['transmitter'].transmit(t.StopInstance("log_printer"))
    print("test")
    while(1):
        r.publish(t.PollUpdate(0.1))
        r.update()
        time.sleep(0.1)


start()
