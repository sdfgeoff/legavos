""" This is a standin module for micropython's machine module. It allow
redirecting input/output to a simulated device when running the simulation
locally"""

class PWM:
    def __init__(self, pin):
        self.pin = pin

    def duty(self, val=None):
        if val is not None:
            assert isinstance(val, int)
        #print("Set servo", self.pin.num, "duty to", val)

    def freq(self, val=None):
        if val is not None:
            assert isinstance(val, int)
        #print("Set servo", self.pin.num, "freq to", val)


class Pin:
    def __init__(self, pin_number):
        self.num = pin_number
