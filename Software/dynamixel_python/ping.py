import serial
import time
from dynamixel import protocol2, servo, servodata

import logging
logging.basicConfig(level=logging.DEBUG)
#logging.getLogger('dynamixel.protocol2').setLevel(logging.CRITICAL)

uart = serial.Serial('/dev/ttyUSB0', 115200, timeout=1.0)
bus = protocol2.Protocol2Bus(uart)


print("Waiting for arduino to boot")
time.sleep(2.0)

# Find the servo, identify it and formalize the connection
for address in [ord('T')]:
    print("Scanning {}/{}".format(address, 254), end="\n")
    servo_model = bus.ping(address)
    if servo_model is not None:
        print("FOUND")
