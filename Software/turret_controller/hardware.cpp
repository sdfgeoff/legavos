#include <Arduino.h>

uint8_t manual_divider = 0;
uint8_t servo_pwm_output = 94;

void _setup_servo_pwm(){
  /* Tilt servo is on pin D3. This corresponds to PD3
   * This is connected to timer/counter 2 compare output B (aka PWM)
   * The aim is to generate a servo signal (20ms period, 0.5-2.5ms pulse width)
   * on this pin.
   * Unfortunately the timer connected to this pin is only 8bit, so 
   * means theres only a couple places the servo can move to across it's
   * range. were the timer used in the obvious way. 
   * 
   * However, we know that the duty cycle is always small compared to
   * the period, and the pin can just be "off" for 80% of it's time.
   * As a result we can run the clock faster and mantain an external
   * overflow count that we use to enable/disable the PWM.
   * 
   * This means that:
   *  - The PWM is generated in hardware, so it is still high accuracy
   *  - The clock is fast, so the signal is precise.
   * 
   * The timers input clock is 32Khz. We can mantain a maximum of a 4 times
   * downscale in software (which means a single timer overflow is 25% of
   * the overflow time), so if we have a 128 times prescalare in hardware,
   * we have:
   * output_period = 32000 / 128 / 4  // The 4 is the software downscale
   *               = 62.5Hz
   * This is close enough to 50Hz that most servos should work fine.
   * 
   * The clock overflow is at 250Hz (4ms), and with an 8 bit division
   * this comes to 0.015625ms per tick
   * So to achieve a servo output (0.5ms to 2.5ms) the duty cycle
   * should be between 32 and 160.
   * This normally maps to an angualar range of 180 degrees, so we have
   * an angular resolution of about 1.4 degrees.
   */
  
  
  // Make pin PD3 into an output pin
  DDRD |= (1 << DDD3);

  // Phase correct PWM mode has waveform generation mode (WGM) set to 
  // 0 (WGM2)
  // 0 (WGM1)
  // 1 (WGM0)
  TCCR2A |= (1 << WGM20);
  //TCCR2A |= (1 << WGM21);
  //TCCR2B |= (1 << WGM22);

  // Prescale the clock by 128 so the overflow is at 250Hz
  TCCR2B |= (1<< CS22) + (1<< CS20);


  // If the timer is in fast PWM mode, set the PWM pin high when the timer is at the bottom, 
  // and set the PWM pin low when the timer is at the target place
  TCCR2A |= (1 << COM2B1); 

  // Set the place in the counter where the PWM will switch off.
  OCR2B = 0;

  // Enable interrupt on timer overflow.
  TIMSK2 |= (1 << TOIE2);

  sei();
}


ISR(TIMER2_OVF_vect){
  // To achieve a longer PWM period, this runs when the counter ticks over and sets
  // and unsets the PWM. This effectively doubles the period

  manual_divider += 1;
  
  if (manual_divider == 4){
    OCR2B = servo_pwm_output;
    manual_divider = 0;
  } else {
    OCR2B = 0;
  }
}

// Sets the angle from between 0 and 180 degrees
void set_servo_angle(uint8_t angle) {
  servo_pwm_output = map(angle, 0, 180, 32, 160);
}

uint8_t read_servo_angle() {
  return map(servo_pwm_output, 32, 160, 0, 180);
}


void init_hardware() {
  _setup_servo_pwm();
}
