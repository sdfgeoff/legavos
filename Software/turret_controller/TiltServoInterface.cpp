#include "TiltServoInterface.h"
#include <Arduino.h>
#include "hardware.h"

TiltServoInterface::TiltServoInterface(uint8_t dynamixel_address, uint16_t dynamixel_model_number, uint8_t dynamixel_software_version){
  {
    address = dynamixel_address;
    model_number = dynamixel_model_number;
    software_version = dynamixel_software_version;
  };
}

void TiltServoInterface::run() {};

void TiltServoInterface::handle_packet(dynamixel_parser_t* parser, uint8_t* output_buffer, uint16_t output_buffer_len, uint16_t* output_buffer_written_len){
  uint8_t params[20] = {0};
  uint8_t written_params = 0;

  if (parser->address == this->address) {
    if (parser->instruction == DYNAMIXEL_INSTRUCTION_PING){
      // Respond to pings
      params[0] = DYNAMIXEL_RESULT_OK;
      params[1] = model_number%256;
      params[2] = model_number>>8;
      params[3] = software_version;
      written_params = 4;
    }
    
    // TODO: Error if num_parameters is incorrect rather than blindly read
    if (parser->instruction == DYNAMIXEL_INSTRUCTION_WRITE){
      uint16_t target_register = parser->parameters[0];
      target_register += parser->parameters[1] << 8;

      switch (target_register) {
        case 30:
          {
            uint16_t target_value = parser->parameters[2];
            target_value += parser->parameters[3] << 8;
            uint16_t servo_value = map(target_value, 0, 1024, 0, 180);
            set_servo_angle(servo_value);
            params[0] = DYNAMIXEL_RESULT_OK;
            written_params = 1;
            break;
          }
        default:
          {
            params[0] = DYNAMIXEL_ACCESS_ERROR;
            written_params = 1;
            break;
          }
      }

    } else if (parser->instruction == DYNAMIXEL_INSTRUCTION_READ){
      uint16_t target_register = parser->parameters[0];
      target_register += parser->parameters[1] << 8;

      switch (target_register) {
        case 0:
          {
            params[0] = DYNAMIXEL_RESULT_OK;
            params[1] = model_number%256;
            params[2] = model_number>>8;
            written_params = 3;
            break;
          }
        case 2:
          {
            params[0] = DYNAMIXEL_RESULT_OK;
            params[1] = software_version;
            written_params = 2;
            break;
          }
        case 3:
          {
            params[0] = DYNAMIXEL_RESULT_OK;
            params[1] = address;
            written_params = 2;
            break;
          }
        case 30:
          {
            uint16_t actual_value = map(read_servo_angle(), 0, 180, 0, 1024);
            params[0] = DYNAMIXEL_RESULT_OK;
            params[1] = actual_value%256;
            params[2] = actual_value >> 8;
            written_params = 3;
            break;
          }
          
        default:
          {
            params[0] = DYNAMIXEL_ACCESS_ERROR;
            written_params = 1;
            break;
          }
      }
    }
  }

  if (written_params != 0) {
    dynamixel_parser_construct_packet(
      output_buffer, output_buffer_len, // Write the packet into here
      address,
      DYNAMIXEL_INSTRUCTION_STATUS, // This is a response packet
      params, written_params, output_buffer_written_len
    );
  }
}



  
