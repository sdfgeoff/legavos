

#include "TiltServoInterface.h"
#include "Gun.h"
#include "GunInterface.h"
#include "hardware.h"

#include "src/SoftwareSerialWithHalfDuplex/SoftwareSerialWithHalfDuplex.h"
#include "src/dynamixel_host/dynamixel_parser.h"
#include "src/drv8833/drv8833.h"

#define BAUD_RATE 57600

#define LED_PIN 13

#define GUN_PIN_1 5
#define GUN_PIN_2 6

#define LOADER_PIN_1 9
#define LOADER_PIN_2 10

#define TILT_SERVO_PIN 3

#define SOFTWARE_SERIAL_PIN 4

#define TILT_SERVO_ADDRESS 0x54
#define GUN_ADDRESS 0x47


void setup() {
  // Nothing in here
}


void loop() {  
  // Initilization when we can do allocations etc.
  Serial.begin(BAUD_RATE);
  Serial.println("Beginning Boot");
  dynamixel_parser_t Parser = {};
  dynamixel_parser_create(&Parser, 30);

  SoftwareSerialWithHalfDuplex serial_port = SoftwareSerialWithHalfDuplex(SOFTWARE_SERIAL_PIN,SOFTWARE_SERIAL_PIN,false,false);
  serial_port.begin(BAUD_RATE);
  Serial.print("Dynamixel interface open on pin ");
  Serial.println(SOFTWARE_SERIAL_PIN);

  init_hardware();
  DRV8833 gun_motor = DRV8833(GUN_PIN_1, GUN_PIN_2);
  DRV8833 loader_motor = DRV8833(LOADER_PIN_1, LOADER_PIN_2);
  Gun gun = Gun(&gun_motor, &loader_motor);

  TiltServoInterface tilt_servo_interface = TiltServoInterface(TILT_SERVO_ADDRESS, 18178, 0x01);
  Serial.print("Tilt Servo available at address ");
  Serial.println(TILT_SERVO_ADDRESS);
  
  GunInterface gun_interface = GunInterface(&gun, GUN_ADDRESS, 18177, 0x01);
  Serial.print("Gun available at address ");
  Serial.println(GUN_ADDRESS);
  
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);
  Serial.println("Boot Complete");
  

  while(1){
    while (serial_port.available() > 0) {
      uint8_t new_data = serial_port.read();
      dynamixel_status_t result = dynamixel_parser_add_byte(&Parser, new_data);
       
      if (result == DYNAMIXEL_PACKET_READY) {
        digitalWrite(LED_PIN, LOW);
        uint8_t outgoing_packet_buffer[30] = {0};
        uint16_t packet_length = 0;
        
        tilt_servo_interface.handle_packet(&Parser, outgoing_packet_buffer, sizeof(outgoing_packet_buffer), &packet_length);
        gun_interface.handle_packet(&Parser, outgoing_packet_buffer, sizeof(outgoing_packet_buffer), &packet_length);

        if (packet_length > 0) {
          serial_port.write(outgoing_packet_buffer, packet_length);
        }
        
        dynamixel_parser_clear(&Parser);
      }
    }
  
    gun.run();
    gun_interface.run();
    tilt_servo_interface.run();  
  }
}
