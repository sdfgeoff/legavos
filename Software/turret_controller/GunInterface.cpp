#include "GunInterface.h"

GunInterface::GunInterface(Gun* _gun, uint8_t dynamixel_address, uint16_t dynamixel_model_number, uint8_t dynamixel_software_version){
  {
    gun = _gun;
    address = dynamixel_address;
    model_number = dynamixel_model_number;
    software_version = dynamixel_software_version;
  };
}

void GunInterface::run() {};

void GunInterface::handle_packet(dynamixel_parser_t* parser, uint8_t* output_buffer, uint16_t output_buffer_len, uint16_t* output_buffer_written_len){
  uint8_t params[20] = {0};
  uint8_t written_params = 0;

  if (parser->address == this->address) {
    if (parser->instruction == DYNAMIXEL_INSTRUCTION_PING){
      // Respond to pings
      params[0] = DYNAMIXEL_RESULT_OK;
      params[1] = model_number%256;
      params[2] = model_number>>8;
      params[3] = software_version;
      written_params = 4;
    }
    
    // TODO: Error if num_parameters is incorrect rather than blindly read
    if (parser->instruction == DYNAMIXEL_INSTRUCTION_WRITE){
      uint16_t target_register = parser->parameters[0];
      target_register += parser->parameters[1] << 8;

      switch (target_register) {
        case 30:
          {
            gun_mode_t gun_mode = (gun_mode_t)parser->parameters[2];
            // TODO: check is valid mode
            gun->set_mode(gun_mode);
            params[0] = DYNAMIXEL_RESULT_OK;
            written_params = 1;
            break;
          }
        case 31:
          {
            gun->set_shots(parser->parameters[2]);
            params[0] = DYNAMIXEL_RESULT_OK;
            written_params = 1;
            break;
          }
        default:
          {
            params[0] = DYNAMIXEL_ACCESS_ERROR;
            written_params = 1;
            break;
          }
      }
      

    } else if (parser->instruction == DYNAMIXEL_INSTRUCTION_READ){
      uint16_t target_register = parser->parameters[0];
      target_register += parser->parameters[1] << 8;

      switch(target_register){
        case 0:
          {
            params[0] = DYNAMIXEL_RESULT_OK;
            params[1] = model_number%256;
            params[2] = model_number>>8;
            written_params = 3;
            break;
          }
        case 2:
          {
            params[0] = DYNAMIXEL_RESULT_OK;
            params[1] = software_version;
            written_params = 2;
            break;
          } 
        case 3:
          {
            params[0] = DYNAMIXEL_RESULT_OK;
            params[1] = address;
            written_params = 2;
            break;
          }
        case 30:
          {
            uint8_t gun_mode = gun->get_mode();
            params[0] = DYNAMIXEL_RESULT_OK;
            params[1] = gun_mode;
            written_params = 2;
            break;
          }
        case 31:
          {
            uint8_t shots = gun->get_shots();
            params[0] = DYNAMIXEL_RESULT_OK;
            params[1] = shots;
            written_params = 2;
            break;
          }
        default:
          {
            params[0] = DYNAMIXEL_ACCESS_ERROR;
            written_params = 1;
            break;
          }
      }

    }
  }

  if (written_params != 0) {
    dynamixel_parser_construct_packet(
      output_buffer, output_buffer_len, // Write the packet into here
      address,
      DYNAMIXEL_INSTRUCTION_STATUS, // This is a response packet
      params, written_params, output_buffer_written_len
    );
  }
}



  
