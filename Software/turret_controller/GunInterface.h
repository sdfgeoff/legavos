#include <stdint.h>

#include "Gun.h"
#include "src/dynamixel_host/dynamixel_parser.h"

class GunInterface {
  public:
    /* Construct a DRV8833 object. The two pins
     * need to be PWM capable */
    GunInterface(Gun* gun, uint8_t dynamixel_address, uint16_t dynamixel_model_number, uint8_t dynamixel_software_version);
    void handle_packet(dynamixel_parser_t* parser, uint8_t* output_buffer, uint16_t output_buffer_len, uint16_t* output_buffer_written_len);
    void run();
    
  private:
    Gun* gun;
    uint8_t address;
    uint16_t model_number;
    uint16_t software_version;
};
