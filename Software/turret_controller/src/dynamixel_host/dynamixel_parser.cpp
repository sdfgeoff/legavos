#include "dynamixel_parser.h"
#include "dynamixel_crc.h"
#include "dynamixel_protocol_defs.h"

#include <stdio.h>
#include <stdlib.h>


uint8_t DYNAMIXEL_HEADER[] = {0xFF, 0xFF, 0xFD, 0x00};
#define DYNAMIXEL_HEADER_SIZE 4  // Size of above header
#define DYNAMIXEL_METADATA_SIZE 3 // Size for address and size information
#define DYNAMIXEL_CRC_SIZE 2// Size for address and size information




dynamixel_status_t dynamixel_parser_create(dynamixel_parser_t* parser, uint16_t buffer_size) {
    parser->_raw_buffer = NULL;
    parser->_raw_buffer = (uint8_t*)malloc(buffer_size);
    if (parser->_raw_buffer == NULL) {
        return DYNAMIXEL_ERROR;
    }

    // Set up the things that can be dealt with as blind references into the
    // packet
    parser->_raw_buffer_len = buffer_size;
    parser->parameters = parser->_raw_buffer + 8;

    dynamixel_parser_clear(parser);

    return DYNAMIXEL_OK;
}


dynamixel_status_t dynamixel_parser_destroy(dynamixel_parser_t* parser) {
    free(parser->_raw_buffer);
    parser->_raw_buffer = NULL;
    parser->_raw_buffer_len = 0;
    parser->parameters = NULL;

    return DYNAMIXEL_OK;
}



dynamixel_status_t dynamixel_parser_clear(dynamixel_parser_t* parser) {
    parser->_raw_buffer_write_address = 0;
    parser->_crc = 0;
    parser->_expected_packet_length = 0;
    parser->num_parameters = 0;

    parser->address = 0;
    parser->instruction = (dynamixel_instruction_t)0;

    parser->data_valid = false;

    return DYNAMIXEL_OK;
}


/* Appends a byte without checking validity, but does check array length etc. */
dynamixel_status_t _dynamixel_parser_do_byte_append(dynamixel_parser_t* parser, uint8_t new_byte) {
    if (parser->_raw_buffer_write_address < parser->_raw_buffer_len){
        parser->_raw_buffer[parser->_raw_buffer_write_address] = new_byte;
        parser->_raw_buffer_write_address += 1;
        return DYNAMIXEL_OK;
    } else {
        dynamixel_parser_clear(parser);
        return DYNAMIXEL_BUFFER_LIMIT_REACHED;
    }
}


dynamixel_status_t dynamixel_parser_add_byte(dynamixel_parser_t* parser, uint8_t new_byte) {
    if (parser->_raw_buffer_write_address < DYNAMIXEL_HEADER_SIZE) {
        if (new_byte == DYNAMIXEL_HEADER[parser->_raw_buffer_write_address]){
            parser->_crc = dynamixel_update_crc(parser->_crc, &new_byte, 1);
            return _dynamixel_parser_do_byte_append(parser, new_byte);
        } else {
            return dynamixel_parser_clear(parser);
        }
    }

    // Receive metadata
    if (parser->_raw_buffer_write_address < DYNAMIXEL_HEADER_SIZE + DYNAMIXEL_METADATA_SIZE){
        parser->_crc = dynamixel_update_crc(parser->_crc, &new_byte, 1);
        return _dynamixel_parser_do_byte_append(parser, new_byte);
    } else if (parser->_raw_buffer_write_address == DYNAMIXEL_HEADER_SIZE + DYNAMIXEL_METADATA_SIZE) {
        parser->_crc = dynamixel_update_crc(parser->_crc, &new_byte, 1);
        dynamixel_status_t response = _dynamixel_parser_do_byte_append(parser, new_byte);
        parser->address = parser->_raw_buffer[4];
        parser->_expected_packet_length = parser->_raw_buffer[5];
        parser->_expected_packet_length += parser->_raw_buffer[6] << 8;
        parser->instruction = (dynamixel_instruction_t)(parser->_raw_buffer[7]);
        parser->num_parameters = parser->_expected_packet_length - 3;
        return response;
    }

    // TODO: remove byte stuffing on-the-fly. When detected, reduce the write expected length by one
    // so that the end detection still works.
    uint16_t total_length = DYNAMIXEL_HEADER_SIZE + DYNAMIXEL_METADATA_SIZE + parser->_expected_packet_length;
    if (parser->_raw_buffer_write_address <= total_length - DYNAMIXEL_CRC_SIZE - 1) {
        // Receiving normal parameters
        parser->_crc = dynamixel_update_crc(parser->_crc, &new_byte, 1);
        return _dynamixel_parser_do_byte_append(parser, new_byte);
    } else if (parser->_raw_buffer_write_address < total_length - 1) {
        // Receiving CRC: don't crc the crc
        return _dynamixel_parser_do_byte_append(parser, new_byte);

    } else if (parser->_raw_buffer_write_address == total_length - 1) {
        // We've now got all the data!
        dynamixel_status_t response = _dynamixel_parser_do_byte_append(parser, new_byte);
        if (response != DYNAMIXEL_OK){
            return response;
        }
        uint16_t expected_crc = parser->_raw_buffer[total_length-2];
        expected_crc += parser->_raw_buffer[total_length-1] << 8;
        if (parser->_crc == expected_crc) {
            parser->data_valid = true;
            return DYNAMIXEL_PACKET_READY;
        } else {
            dynamixel_parser_clear(parser);
            return DYNAMIXEL_PACKET_CRC_ERROR;
        }

    } else {
        // We got too much data. Something should have flushed the buffer by now.
        // Emit an error and hope something clears it...
        return DYNAMIXEL_ERROR;
    }
}




dynamixel_status_t _buffer_add_byte(uint8_t* buffer, uint16_t buffer_len, uint16_t position, uint8_t new_byte) {
    if (position < buffer_len) {
        buffer[position] = new_byte;
        return DYNAMIXEL_OK;
    } else {
        return DYNAMIXEL_BUFFER_LIMIT_REACHED;
    }
}

dynamixel_status_t dynamixel_parser_construct_packet(uint8_t* buffer, uint16_t buffer_len, uint8_t address, dynamixel_instruction_t instruction, uint8_t* params, uint16_t params_length, uint16_t* length_written){
    // We can go through the whole log and pretend it fits into the buffer
    // because the _buffer_add_byte guards against writing if the limit is
    // exceeded. Then we can return the final write because that will also fail
    // if preceeding ones have failed.
    _buffer_add_byte(buffer, buffer_len, 0, 0xFF);
    _buffer_add_byte(buffer, buffer_len, 1, 0xFF);
    _buffer_add_byte(buffer, buffer_len, 2, 0xFD);
    _buffer_add_byte(buffer, buffer_len, 3, 0x00);
    _buffer_add_byte(buffer, buffer_len, 4, address);

    _buffer_add_byte(buffer, buffer_len, DYNAMIXEL_HEADER_SIZE + DYNAMIXEL_METADATA_SIZE, (uint8_t)instruction);


    uint16_t i = 0;
    for (i=0; i<params_length; i+=1) {
        // TODO add byte stuffing in here somewhere
        dynamixel_status_t status = _buffer_add_byte(buffer, buffer_len, DYNAMIXEL_HEADER_SIZE + DYNAMIXEL_METADATA_SIZE + i + 1, params[i]);
        if (status != DYNAMIXEL_OK){
            // This early return is important as it prevents the CRC from running if there was an error filling the buffer
            return status;
        }
    }

    uint16_t total_length = params_length + DYNAMIXEL_METADATA_SIZE; // TODO this should include byte stuffing
    _buffer_add_byte(buffer, buffer_len, 5, (uint8_t)(total_length % 256));
    _buffer_add_byte(buffer, buffer_len, 6, (uint8_t)(total_length >> 8));

    // TODO: DON'T DO THIS IF THE BUFFER WAS ALREADY FILLED UP, OTHERWISE THIS IS AN ERRORNEOUS READ
    uint16_t crc = dynamixel_update_crc(0, buffer, params_length + 8);


    _buffer_add_byte(buffer, buffer_len, DYNAMIXEL_HEADER_SIZE + DYNAMIXEL_METADATA_SIZE + 1 + i, (uint8_t)(crc % 256));
    dynamixel_status_t status = _buffer_add_byte(buffer, buffer_len, DYNAMIXEL_HEADER_SIZE + DYNAMIXEL_METADATA_SIZE + 2 + i, (uint8_t)(crc >> 8));
    *length_written = DYNAMIXEL_HEADER_SIZE + DYNAMIXEL_METADATA_SIZE + DYNAMIXEL_CRC_SIZE + i + 1;
    return status;
}

