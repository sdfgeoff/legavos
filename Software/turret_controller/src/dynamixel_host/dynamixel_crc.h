/* Dynamixel packets contain a CRC to check for transmission errors.
 *
 * This file is sourced from
 * http://emanual.robotis.com/docs/en/dxl/crc/  in May 2020
 * and should work with all protocol 2.0 servos */
#ifndef __DYNAMIXEL_CRC__
#define __DYNAMIXEL_CRC__
#include <stdint.h>

uint16_t dynamixel_update_crc(uint16_t crc_accum, uint8_t *data_blk_ptr, uint16_t data_blk_size);

#endif
