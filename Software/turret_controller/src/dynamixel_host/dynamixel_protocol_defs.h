#ifndef __DYNAMIXEL_PROTOCOL_DEFS_H__
#define  __DYNAMIXEL_PROTOCOL_DEFS_H__

typedef enum {
    DYNAMIXEL_INSTRUCTION_PING = 0x01,
    DYNAMIXEL_INSTRUCTION_READ = 0x02,
    DYNAMIXEL_INSTRUCTION_WRITE = 0x03,
    DYNAMIXEL_INSTRUCTION_STATUS = 0x55, // Status packets are normal packets but with this instruction
} dynamixel_instruction_t;


// These are not in an enum so that you can pass their address when sending
// a response
const uint8_t DYNAMIXEL_RESULT_OK = 0x00;
const uint8_t DYNAMIXEL_RESULT_ALERT = 1 << 7;
const uint8_t DYNAMIXEL_RESULT_FAIL = 0x01;
const uint8_t DYNAMIXEL_INSTRUCTION_ERROR = 0x02;
const uint8_t DYNAMIXEL_CRC_ERROR = 0x03;
const uint8_t DYNAMIXEL_DATA_RANGE_ERROR = 0x04;
const uint8_t DYNAMIXEL_DATA_LENGTH_ERROR = 0x05;
const uint8_t DYNAMIXEL_DATA_LIMIT_ERROR = 0x06;
const uint8_t DYNAMIXEL_ACCESS_ERROR = 0x07;



#endif
