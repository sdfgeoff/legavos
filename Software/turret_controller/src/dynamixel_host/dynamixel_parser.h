#ifndef __DYNAMIXEL_PACKET__
#define __DYNAMIXEL_PACKET__

/* Dynamixel servos use a serial protocol for control. This
 * file handles parsing and validity checking the wire part of the protocol.
 * It is designed to work iteratively, processing each new byte as it arrives.
 *
 * Normal use is:
 * 1) Initilize a dynamixel_parser_t with dynamixel_create_parser()
 * 2) Whenever you get a byte from serial, call dynamixel_parser_add_byte()
 *    If a packet is a available, the function will return DYNAMIXEL_PACKET_READY
 *    you can then read the packet by accessing the contents of the parser
 *    struct
 * 3) After dealing with the packet, call dynamixel_parser_clear(). This must
 *    be called before calling add_byte again.
 *
 *
 *
 * TODO:
 *  - Add in De-byte-stuffing
 */


#include <stdint.h>
#include "dynamixel_protocol_defs.h"



typedef struct {
    uint8_t* _raw_buffer;
    uint16_t _raw_buffer_len;
    uint16_t _raw_buffer_write_address;
    uint16_t _expected_packet_length;
    uint16_t _crc;

    bool data_valid; // True if the other public fields contain useful data
    uint8_t address;
    dynamixel_instruction_t instruction;
    uint16_t num_parameters;
    uint8_t* parameters;

} dynamixel_parser_t;

typedef enum {
    DYNAMIXEL_OK = 0x00,
    DYNAMIXEL_ERROR = 0x01,
    DYNAMIXEL_PACKET_READY = 0x02,
    DYNAMIXEL_PACKET_CRC_ERROR = 0x04,
    DYNAMIXEL_BUFFER_LIMIT_REACHED = 0x05,
} dynamixel_status_t;


/* Allocates space for a parser with the supplied buffer length */
dynamixel_status_t dynamixel_parser_create(dynamixel_parser_t* parser, uint16_t buffer_size);

/* Frees the buffer inside the parser. After this point, don't use it! It
 * will always return DYNAMIXEL_BUFFER_LIMIT_REACHED */
dynamixel_status_t dynamixel_parser_destroy(dynamixel_parser_t* parser);

/* Adds a byte to the parser. If there is a complete packet in the buffer
 * it will return DYNAMIXEL_PACKET_READY.
 * At this point you can use the various public methods of the struct
 * to access the data in the packet.
 * After dealing with teh packet, call "dynamixel_clear_parser" to reset it.
 * Bytes added on top of competed buffers will not be recovered.
 */
dynamixel_status_t dynamixel_parser_add_byte(dynamixel_parser_t* parser, uint8_t new_byte);

/* Clears the internal buffer of the parser, resetting it to a "blank" state.
 */
dynamixel_status_t dynamixel_parser_clear(dynamixel_parser_t* parser);



/* Construct a packet ready for transmission */
dynamixel_status_t dynamixel_parser_construct_packet(uint8_t* buffer, uint16_t buffer_len, uint8_t address, dynamixel_instruction_t instruction, uint8_t* params, uint16_t params_length, uint16_t* length_written);

#endif
