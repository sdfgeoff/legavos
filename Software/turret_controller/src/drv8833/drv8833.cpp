#include "drv8833.h"
#include <Arduino.h>

DRV8833::DRV8833(uint8_t pin_a, uint8_t pin_b) {
  pinMode(pin_a, OUTPUT);
  pinMode(pin_b, OUTPUT);
  {
    pin_1 = pin_a;
    pin_2 = pin_b;
  };
}

void DRV8833::set_speed(int8_t speed) {
  uint8_t abs_speed = abs(speed);  // Input speed is between 127 and -127, PWM output is between -255 and 255
  if (speed > 0) {
    analogWrite(pin_1, abs_speed * 2);
    digitalWrite(pin_2, LOW);
  } else {
    digitalWrite(pin_1, LOW);
    analogWrite(pin_2, abs_speed * 2);
  }
}

void DRV8833::set_brake(uint8_t brake) {
  analogWrite(pin_1, brake);
  analogWrite(pin_2, brake);
}
