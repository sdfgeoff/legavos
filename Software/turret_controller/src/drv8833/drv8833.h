#ifndef __DRV8833_H__
#define __DRV8833_H__

#include <stdint.h>

class DRV8833 {
  public:
    /* Construct a DRV8833 object. The two pins
     * need to be PWM capable */
    DRV8833(uint8_t pin_1, uint8_t pin_2);

    /* Set the speed between -100 and +100 where
    -100 is full backwards and +100 is full forwards */
    void set_speed(int8_t speed);
    void set_brake(uint8_t brake);

  private:
    uint8_t pin_1;
    uint8_t pin_2;
};


#endif
