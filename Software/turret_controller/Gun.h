#ifndef __GUN_H__
#define __GUN_H__

#include "src/drv8833/drv8833.h"


typedef enum {
  GUN_MODE_INACTIVE = 0x00,
  GUN_MODE_ARMED = 0x01,
  GUN_MODE_LOAD = 0x02,
  GUN_MODE_UNLOAD = 0x03,
} gun_mode_t;


class Gun {
  public:
    /* Construct a DRV8833 object. The two pins
     * need to be PWM capable */
    Gun(DRV8833* loader_motor, DRV8833* gun_motor);

    void run();

    void set_mode(gun_mode_t mode);
    gun_mode_t get_mode();

    void set_shots(uint8_t shots);
    uint8_t get_shots();
    
    
  private:
    gun_mode_t gun_mode;

    uint8_t shots_remaining;

    DRV8833* loader_motor;
    DRV8833* gun_motor;
    uint32_t last_shot_time;
};
//
//
#endif
