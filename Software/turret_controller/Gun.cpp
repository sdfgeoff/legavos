#include "Gun.h"
#include <Arduino.h>


#define LOADER_FORWARD_SPEED -100 // Speed at which the loader runs during normal operation
#define LOADER_BACKWARD_SPEED 100  // Speed at which the loader runs

#define GUN_FORWARD_SPEED -127 // Speed at which the gun runs while shooting
#define GUN_BACKWARD_SPEED -100 // Speed at which the gun runs when trying to unjam

// Because there is no sensor detecting when the gun actually fires, 
// we need some way to approximate if a shot has actually been fired.
// This is done by timing how long the gun motor is active for.
// With a 100RPM N20 motor, the "max power" speed is 60RPM. Conveniently
// this works out to one shot per second. So a well-configured gun will
// shoot once per second, or thereabouts.
#define SHOT_TIME 1000  



Gun::Gun(DRV8833* _loader_motor, DRV8833* _gun_motor) {
  {
    loader_motor = _loader_motor;
    gun_motor = _gun_motor;
    gun_mode = GUN_MODE_INACTIVE;
    shots_remaining = 0;
  };
}

void Gun::run() {
  if (gun_mode == GUN_MODE_INACTIVE) {
    loader_motor->set_speed(0);
    gun_motor->set_speed(0);
  } else if (gun_mode == GUN_MODE_LOAD){
    loader_motor->set_speed(LOADER_FORWARD_SPEED);
    gun_motor->set_speed(0);
  } else if (gun_mode == GUN_MODE_UNLOAD){
    loader_motor->set_speed(LOADER_BACKWARD_SPEED);
    gun_motor->set_speed(0);
  } else if (gun_mode == GUN_MODE_ARMED) {
    if (shots_remaining == 0){
      loader_motor->set_speed(0);
      gun_motor->set_speed(0);
    } else {
      uint32_t now = millis();
      if (now - last_shot_time > SHOT_TIME) {
        shots_remaining -= 1;
        last_shot_time = now;
      }
      loader_motor->set_speed(LOADER_FORWARD_SPEED);
      gun_motor->set_speed(GUN_FORWARD_SPEED);
    }
  } else {
    // Invalid mode set to idle
    gun_mode = GUN_MODE_INACTIVE;
  }

}

void Gun::set_mode(gun_mode_t mode){
  gun_mode = mode;
  shots_remaining = 0;
}


gun_mode_t Gun::get_mode(){
  return gun_mode;
}

void Gun::set_shots(uint8_t shots){
  if (gun_mode == GUN_MODE_ARMED){
    if (shots_remaining == 0){
      last_shot_time = millis();
    }
    shots_remaining = shots;
  }
}

uint8_t Gun::get_shots() {
  return shots_remaining;
}
