/* Direct control over the hardware */

// Tilt servo is on pin D3. This corresponds to PD3
// This is connected to timer/counter 2 compare output (aka PWM)
// This needs to be configured for a PWM output of 50Hz

// Loader is on pin D5 and D6
// this corresponds to PD5 and PD6
// This is connected to timer/counter 0 output A and timer/counter 0 output B
// This needs to be configured for a PWM frequency of 10Khz or thereabouts

// Gun motor is on pin D9 and D10
// this corresponds to PB1 and PB2
// THese are connected to timer/counter 1 output A and timer/counter 1 output B
// This needs to be configured for a PWM frequency of 10Khz or thereabouts



void init_hardware();

void set_servo_angle(uint8_t angle);
uint8_t read_servo_angle();
