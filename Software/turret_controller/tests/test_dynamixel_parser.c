#include "unity.h"
#include "dynamixel_parser.h"


uint8_t valid_header[] = {0xFF, 0xFF, 0xFD, 0x00};
uint8_t valid_ping_id_8_packet[] = {0xff, 0xff, 0xfd, 0x00, 0x08, 0x03, 0x00, 0x01, 0x19, 0xfa};
uint8_t invalid_ping_id_8_packet[] = {0xff, 0xff, 0xfd, 0x00, 0x08, 0x03, 0x00, 0x02, 0x19, 0xfa};

void test_check_newly_created_has_no_data(void) {
    dynamixel_parser_t parser = {};
    TEST_ASSERT_EQUAL_MESSAGE(
        dynamixel_parser_create(&parser, 20),
        DYNAMIXEL_OK,
        "Creation of parser failed"
    );

    TEST_ASSERT_EQUAL_MESSAGE(
        parser.data_valid,
        false,
        "Data inited as valid? How could a packet arrive already!"
    );
}


void test_adding_valid_data_adds_data(void) {
    dynamixel_parser_t parser = {};
    TEST_ASSERT_EQUAL_MESSAGE(dynamixel_parser_create(&parser, 20), DYNAMIXEL_OK, "Creation of parser failed");


    TEST_ASSERT_EQUAL(dynamixel_parser_add_byte(&parser, valid_header[0]), DYNAMIXEL_OK);
    TEST_ASSERT_EQUAL_MESSAGE(1, parser._raw_buffer_write_address, "received byte mishandled (1)");

    TEST_ASSERT_EQUAL(dynamixel_parser_add_byte(&parser, valid_header[1]), DYNAMIXEL_OK);
    TEST_ASSERT_EQUAL_MESSAGE(2, parser._raw_buffer_write_address, "received byte mishandled (2)");

    TEST_ASSERT_EQUAL(dynamixel_parser_add_byte(&parser, valid_header[2]), DYNAMIXEL_OK);
    TEST_ASSERT_EQUAL_MESSAGE(3, parser._raw_buffer_write_address, "received byte mishandled (3)");

    TEST_ASSERT_EQUAL(dynamixel_parser_add_byte(&parser, valid_header[3]), DYNAMIXEL_OK);
    TEST_ASSERT_EQUAL_MESSAGE(4, parser._raw_buffer_write_address, "received byte mishandled (4)");
}



void _test_add_buffer(dynamixel_parser_t* parser, uint8_t* buffer, uint16_t buffer_len){
    for (uint16_t i=0; i<buffer_len; i++) {
        TEST_ASSERT_EQUAL_MESSAGE(dynamixel_parser_add_byte(parser, buffer[i]), DYNAMIXEL_OK, "Insertion failed");
    }
}

void test_adding_invalid_header_clears_data(void) {
    dynamixel_parser_t parser = {};
    TEST_ASSERT_EQUAL_MESSAGE(dynamixel_parser_create(&parser, 20), DYNAMIXEL_OK, "Creation of parser failed");

    TEST_ASSERT_EQUAL(dynamixel_parser_add_byte(&parser, valid_header[0]), DYNAMIXEL_OK);
    TEST_ASSERT_EQUAL_MESSAGE(parser._raw_buffer_write_address, 1, "received byte mishandled (1)");

    TEST_ASSERT_EQUAL(dynamixel_parser_add_byte(&parser, 0x98), DYNAMIXEL_OK);
    TEST_ASSERT_EQUAL_MESSAGE(parser._raw_buffer_write_address, 0, "invalid header byte accepted");

    TEST_ASSERT_EQUAL(dynamixel_parser_add_byte(&parser, valid_header[0]), DYNAMIXEL_OK);
    TEST_ASSERT_EQUAL_MESSAGE(parser._raw_buffer_write_address, 1, "received byte mishandled (1a)");
    TEST_ASSERT_EQUAL(dynamixel_parser_add_byte(&parser, valid_header[1]), DYNAMIXEL_OK);
    TEST_ASSERT_EQUAL_MESSAGE(parser._raw_buffer_write_address, 2, "received byte mishandled (2a)");

    TEST_ASSERT_EQUAL(dynamixel_parser_add_byte(&parser, 0x67), DYNAMIXEL_OK);
    TEST_ASSERT_EQUAL_MESSAGE(parser._raw_buffer_write_address, 0, "invalid header byte accepted");
}

void test_packet_declared_ready(void) {
    dynamixel_parser_t parser = {};
    TEST_ASSERT_EQUAL_MESSAGE(dynamixel_parser_create(&parser, 20), DYNAMIXEL_OK, "Creation of parser failed");

    _test_add_buffer(&parser, valid_ping_id_8_packet, sizeof(valid_ping_id_8_packet)-1);
    TEST_ASSERT_EQUAL(dynamixel_parser_add_byte(&parser, valid_ping_id_8_packet[sizeof(valid_ping_id_8_packet)-1]), DYNAMIXEL_PACKET_READY);
    TEST_ASSERT_EQUAL_MESSAGE(parser.data_valid, true, "packet declared ready, but data_valid flag not set");
    TEST_ASSERT_EQUAL_MESSAGE(parser.instruction, DYNAMIXEL_INSTRUCTION_PING, "packet declared ready, instruction incorrect");
    TEST_ASSERT_EQUAL_MESSAGE(parser.num_parameters, 0, "Incorrect number parameters reported");
}


void test_packet_emits_crc_error(void) {
    dynamixel_parser_t parser = {};
    TEST_ASSERT_EQUAL_MESSAGE(dynamixel_parser_create(&parser, 20), DYNAMIXEL_OK, "Creation of parser failed");

    _test_add_buffer(&parser, invalid_ping_id_8_packet, sizeof(invalid_ping_id_8_packet)-1);
    TEST_ASSERT_EQUAL(dynamixel_parser_add_byte(&parser, invalid_ping_id_8_packet[sizeof(valid_ping_id_8_packet)-1]), DYNAMIXEL_PACKET_CRC_ERROR);
}


void test_parser_add_byte_after_destroy(void) {
    dynamixel_parser_t parser = {};
    TEST_ASSERT_EQUAL_MESSAGE(dynamixel_parser_create(&parser, 20), DYNAMIXEL_OK, "Creation of parser failed");
    TEST_ASSERT_EQUAL_MESSAGE(dynamixel_parser_destroy(&parser), DYNAMIXEL_OK, "Destruction of parser failed");

    TEST_ASSERT_EQUAL(dynamixel_parser_add_byte(&parser, valid_header[0]), DYNAMIXEL_BUFFER_LIMIT_REACHED);
}



void test_packet_create_ping(void) {
    uint8_t constructed_packet_buffer[10] = {0};
    uint16_t packet_length = 0;

    TEST_ASSERT_EQUAL_MESSAGE(dynamixel_parser_construct_packet(
        constructed_packet_buffer, sizeof(constructed_packet_buffer),
        0x08, // Address
        DYNAMIXEL_INSTRUCTION_PING,
        NULL,
        0,
        &packet_length
    ), DYNAMIXEL_OK, "failed to construct ping packet");
    TEST_ASSERT_EQUAL_UINT8_ARRAY(valid_ping_id_8_packet, constructed_packet_buffer, sizeof(valid_ping_id_8_packet));
    TEST_ASSERT_EQUAL_MESSAGE(10, packet_length, "packet legnth reported mismatch");
}


void test_dynamixel_parser(void){
    TEST_ASSERT_EQUAL_MESSAGE(
        sizeof(valid_header),
        4,
        "sizeof reporting oncorrect sizes on uint8_t arrays. Probably an architecture issue?"
    );

    RUN_TEST(test_check_newly_created_has_no_data);
    RUN_TEST(test_adding_valid_data_adds_data);
    RUN_TEST(test_adding_invalid_header_clears_data);
    RUN_TEST(test_packet_declared_ready);
    RUN_TEST(test_packet_emits_crc_error);
    RUN_TEST(test_parser_add_byte_after_destroy);
    RUN_TEST(test_packet_create_ping);
}
