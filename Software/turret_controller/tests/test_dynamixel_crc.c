#include "unity.h"
#include "dynamixel_crc.h"

void test_crc_generation(void) {
    uint8_t ping_id_3[] = {0xff, 0xff, 0xfd, 0x00, 0x03, 0x03, 0x00, 0x01};
    TEST_ASSERT_EQUAL_UINT16(58906, dynamixel_update_crc(0, ping_id_3, 8)); //this one will pass

    uint8_t ping_id_8[] = {0xff, 0xff, 0xfd, 0x00, 0x08, 0x03, 0x00, 0x01};
    TEST_ASSERT_EQUAL_UINT16(64025, dynamixel_update_crc(0, ping_id_8, 8)); //this one will pass
}


void test_dynamixel_crc(void){
    RUN_TEST(test_crc_generation);
}
