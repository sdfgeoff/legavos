## The Turret Controller
The turret controller resides in the tilting part of teh turret. It is
handles communication to the main controller - allowing it control over the
turrets functions.
If in future more features are added to the turret, they will probably be
as extensions to the turret controller.

There are three wires that lead to the turret: ground, power and signal. The
signal line uses the [dynamixel 2.0
protocol](http://emanual.robotis.com/docs/en/dxl/protocol2/) and may be
connected to other devices on the robot (eg servos in the leg mechanism)

The turret controller is not a fully fledged "Smart servo" as it does not support
programming the baud rate, ID's etc. Because this is a controller with a specific
purpose and location in the robot it was deemed not worth the effort over
hardcoding these values.

In it's current form the turret controller has has two main purposes: to control
allow the tilt servo to be controlled over the dynamixel bus, and to coordinate
the actions of the gun electronics. As such it appears as two virtual devices.

### Tilt Servo (0x54)
The tilt servo is ID `0x54` - or the ascii character "T" for tilt. It provides
control over the angle of the turret.

#### Control Table

| Name             | Register | Size | RW | Initial Value | Implemented |
|------------------|----------|------|----|---------------|-------------|
| Model Number     | 0x00     | 2    | R  | 18177         |  N          |
| Firmware Version | 0x02     | 1    | R  |               |  N          |
| Bus ID           | 0x03     | 1    | R  | 0x54          |  N          |
| Goal Position    | 0x30     | 2    | RW | 512           |  N          |

#### Details

##### Model Number (0x00)
The model number of the servo. This is `18177` or the two characters `0x47
0x01` (The first character is 'G' for Geoff, the second is 0x01 because it is
the first device I'm working on)

##### Firmware Version (0x02)
This is what version of software is programmed onto the device. Currently
this is "0x01" because the controller is still in initial development....

##### Bus ID (0x03)
Normally this can be used to set what ID the smart-servo appears on. Because
this is a controller for a specific purpose, this number cannot be set and
is permanantly as `0x54` - the ascii letter "T" for "Tilt".

##### Goal Position (0x30)
Nearly all the dynamixel servos have their position control at address 0x30,
so that is why this is located here. Similar to the XL-320 servo, this has a
range of 0-1024. This maps to 180 degrees of motion of the servo.






### Gun Control (0x47)
The gun has two motors (and in the future, possibly some sensors). These
motors are both controlled through device `0x47` - the ascii character "T".

#### Control Table
| Name             | Register | Size | RW | Initial Value | Implemented |
|------------------|----------|------|----|---------------|-------------|
| Model Number     | 0x00     | 2    | R  | 18178         |  N          |
| Firmware Version | 0x02     | 1    | R  |               |  N          |
| Bus ID           | 0x03     | 1    | R  | 0x47          |  N          |
| Gun Mode         | 0x30     | 1    | RW | 0             |  N          |
| Trigger          | 0x31     | 1    | RW | 0             |  N          |

#### Details

##### Model Number
The model number of the servo. This is `18178` or the two characters `0x47
0x02` (The first character is 'G' for Geoff, the second is 0x02 because it is
the second device I'm working on)

##### Firmware Version (0x02)
This is what version of software is programmed onto the device. Currently
this is "0x01" because the controller is still in initial development....

##### Bus ID (0x03)
Normally this can be used to set what ID the smart-servo appears on. Because
this is a controller for a specific purpose, this number cannot be set and
is permanantly as `0x47` - the ascii letter "G" for "Gun".


##### Gun Mode (0x30)
The trigger mode is used to enable/disable the trigger and to control
additional behaviours of the gun motor. Writing to this register will reset
the Trigger register to `0x00`
It can be set to the following:

 - *Inactive* `0x00`: All motors are disabled regardless of the value written
to the Trigger. This prevents all firing of the gun and motion of the gun motor.
 - *Armed* `0x01`: the gun can be fired by writing into  the Trigger register.
 - *Unload* `0x02`: The gun motor will remain disabled, and the loader will
    run backwards to clear the BB's from the tube. After two seconds, the mode
    will revert to "Inactive"
 - *Load* `0x03`: The gun motor will remain disabled and the loader will run
    forwards, feeding BB's into the tube between the loader and the gun.

#### Gun Trigger (0x31)
This register is only useful when the gun mode (0x30) is in the "Armed" state.
In this state, the value of this register will be the number of shots that the
gun fires. After each shot, the gun will decrement this register.
For example, if you set this register to the value `0x03`, the gun will fire
a shot and the the value in this register will be `0x02`. When the value in this
register is changed to zero, the gun will stop shooting.

If the Gun Mode (0x30) is changed, this value is immediately reset to `0x00` to
prevent accidental firing.
