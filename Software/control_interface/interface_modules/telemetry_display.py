""" Displays a table of telemetry in the corner of the HUD """
import os
import pygame
import log
from collections import namedtuple

import telemetry

FONT_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), "bedstead.otf")
FONT_SIZE = 26
FONT = pygame.font.Font(FONT_PATH, FONT_SIZE)


OUTLINE_COLOR = pygame.Color(0, 0, 0)
OUTLINE_WIDTH = 2
LEVEL_COLORS = {
    log.EMERGENCY: pygame.Color(255, 0, 0),
    log.ALERT: pygame.Color(255, 0, 0),
    log.CRITICAL: pygame.Color(255, 0, 0),
    log.ERROR: pygame.Color(255, 0, 0),
    log.WARNING: pygame.Color(255, 128, 0),
    log.NOTICE: pygame.Color(255, 255, 255),
    log.INFO: pygame.Color(255, 255, 255),
    log.DEBUG: pygame.Color(100, 100, 100)
}


OUTLINE_OFFSETS = (
    (OUTLINE_WIDTH, OUTLINE_WIDTH),
    (OUTLINE_WIDTH, -OUTLINE_WIDTH),
    (-OUTLINE_WIDTH, OUTLINE_WIDTH),
    (-OUTLINE_WIDTH, -OUTLINE_WIDTH)
)

TelemetryData = namedtuple("TelemetryData", ("value", "level"))


class TelemetryDisplay:
    def __init__(self):
        self.surface = None
        self.telemetry = {}

    def set_draw_surface(self, event):
        self.surface = event.surface

    def new_telemetry(self, event):
        name_string = telemetry.NAME_LOOKUP[event.key].replace('_', ' ').lower()
        format_function = telemetry.FORMATTERS.get(event.key, lambda x: "{x}?".format(x))
        value_string = format_function(event.value)
        self.telemetry[name_string] = TelemetryData(value_string, event.level)

    def update(self, event):
        if self.surface is not None:
            names = sorted([n for n in self.telemetry.keys()])
            for line_num, name in enumerate(names):
                entry = self.telemetry[name]
                self.draw_telemetry_entry(entry.level, name, entry.value, line_num)


    def draw_telemetry_entry(self, level, name, value, line):
        color = LEVEL_COLORS[level]
        text = "{:15} | {}".format(name, value)

        base_position = (0, FONT_SIZE * line)

        outline_sample = FONT.render(text, True, OUTLINE_COLOR)
        main_sample = FONT.render(text, True, color)
        for i in OUTLINE_OFFSETS:
            self.surface.blit(outline_sample, add_vec(base_position, i))

        self.surface.blit(main_sample, base_position)


def add_vec(a, b):
    return (a[0] + b[0], a[1] + b[1])
