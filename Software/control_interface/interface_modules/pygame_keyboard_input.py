import pygame

import router as r
import topics as t


PAN_SPEED = 1.0
ELEVATION_SPEED = 1.0
PAN_ANGLE = 45
ELEVATION_NEG_ANGLE = 60
ELEVATION_POS_ANGLE = 80


class PygameKeyboardInput:
    """ Generates ControlMotion events from pygame keyboard input """
    def __init__(self, send_rate):
        self.seconds_between_transmit = 1/send_rate
        self.time_since_transmit = 0

        self.speed_multiplier = 1.0

        self.pan = 0
        self.elevation = 0
        self.packet_counter = 0

        self.gun_mode = t.GUN_MODE_INACTIVE
        self.gun_bullet_id = 0

    def update(self, event):
        self.time_since_transmit += event.deltatime
        if self.time_since_transmit > self.seconds_between_transmit:
            self.time_since_transmit -= self.seconds_between_transmit
            self.transmit()

        keys = pygame.key.get_pressed()
        if keys[pygame.K_a]:
            self.pan -= PAN_SPEED * event.deltatime
        if keys[pygame.K_d]:
            self.pan += PAN_SPEED * event.deltatime

        if keys[pygame.K_w]:
            self.elevation -= ELEVATION_SPEED * event.deltatime
        if keys[pygame.K_s]:
            self.elevation += ELEVATION_SPEED * event.deltatime

        if self.pan > 1:
            self.pan = 1
        elif self.pan < -1:
            self.pan = -1

        if self.elevation > 1:
            self.elevation = 1
        elif self.elevation < -1:
            self.elevation = -1


        if keys[pygame.K_1]:
            self.speed_multiplier = 0.2
        if keys[pygame.K_2]:
            self.speed_multiplier = 0.4
        if keys[pygame.K_3]:
            self.speed_multiplier = 0.6
        if keys[pygame.K_4]:
            self.speed_multiplier = 0.8
        if keys[pygame.K_5]:
            self.speed_multiplier = 1.0

        if keys[pygame.K_SPACE] and self.gun_mode == t.GUN_MODE_ARMED:
            self.gun_bullet_id += 1

        if keys[pygame.K_q]:
            self.gun_mode = t.GUN_MODE_ARMED
        elif keys[pygame.K_e]:
            self.gun_mode = t.GUN_MODE_INACTIVE



    def transmit(self):
        keys = pygame.key.get_pressed()

        self.packet_counter += 1
        self.packet_counter = self.packet_counter % 256

        chassis_forwards = 0
        chassis_strafe = 0
        chassis_rotate = 0
        if keys[pygame.K_LEFT]:
            chassis_rotate -= 1
        if keys[pygame.K_RIGHT]:
            chassis_rotate += 1
        if keys[pygame.K_UP]:
            chassis_forwards += 1
        if keys[pygame.K_DOWN]:
            chassis_forwards -= 1

        chassis_forwards *= self.speed_multiplier
        chassis_rotate *= self.speed_multiplier


        turret_pan = self.pan * PAN_ANGLE

        if self.elevation < 0:
            turret_elevation = self.elevation * ELEVATION_NEG_ANGLE
        else:
            turret_elevation = self.elevation * ELEVATION_POS_ANGLE


        # Temporarily change gun mode if certain keys pressed
        gun_mode = self.gun_mode
        if keys[pygame.K_z]:
            gun_mode = t.GUN_MODE_LOAD
        elif keys[pygame.K_x]:
            gun_mode = t.GUN_MODE_UNLOAD

        r.publish(t.ControlPacket(
            self.packet_counter,
            int(chassis_forwards * 100),
            int(chassis_strafe * 100),
            int(chassis_rotate * 100),

            int(turret_pan * 10),
            int(turret_elevation * 10),

            gun_mode,
            self.gun_bullet_id % 256
        ))

