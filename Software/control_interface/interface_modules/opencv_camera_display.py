""" This module uses pygame to fetch video from a webcam or video receiver
dongle """

CAMERA = "/dev/video0" # How to handle this?

import cv2
import numpy
import pygame


class OpenCVCameraDisplay:
    def __init__(self):
        self.surface = None
        self.camera = cv2.VideoCapture(0)

        cam_width = 640
        cam_height = 480

        self.raw_frame = numpy.zeros(shape=(cam_height, cam_width, 3), dtype=numpy.uint8)
        #self.colorspace_frame = numpy.zeros(shape=(cam_height, cam_width, 3), dtype=numpy.uint8)
        #self.rot_frame = numpy.zeros(shape=(cam_height, cam_width, 3), dtype=numpy.uint8)

    def update(self, event):
        ret, frame = self.camera.read(self.raw_frame)

        frame = cv2.cvtColor(self.raw_frame, cv2.COLOR_BGR2RGB)

        frame = numpy.rot90(frame)
        frame = numpy.flipud(frame)

        frame = numpy.rot90(frame, 2)

        image = pygame.surfarray.make_surface(frame)

        if self.surface is not None:
            self.surface.blit(image, (0,0))


    def set_draw_surface(self, event):
        self.surface = event.surface
