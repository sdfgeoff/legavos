""" This module uses pygame to fetch video from a webcam or video receiver
dongle """

CAMERA = "/dev/video0" # How to handle this?

import pygame.camera

pygame.camera.init()


class PygameCameraDisplay:
    def __init__(self, surface):
        self.surface = surface
        self.camera = pygame.camera.Camera("/dev/video0", (640,480))
        self.camera.start()
        
    def update(self, event):
        image = self.camera.get_image()
        self.surface.blit(image, (0,0))
