import sys
import time

import pygame
import pygame.camera

import fixpath

import extra_topics as t
import router as r
import log




pygame.init()


def setup_modules():
    from modules.log_printer import LogPrinter
    r.register_instance(
        "log_printer", LogPrinter,
        {
            t.Log: "print_log",
            t.Telemetry: "print_telemetry"
        }
    )

    from modules.router_log_bridge import RouterLogBridge
    r.register_instance("router_log_bridge", RouterLogBridge, {
        t.StopInstance: "stop_instance",
        t.StartInstance: "start_instance",
        t.MessageMishandled: "message_mishandled",
        t.StartInstanceFailed: "start_instance_failed",
    })

    from modules.udp_bridge import UdpBridgeSender
    r.register_instance("control_transmitter", lambda: UdpBridgeSender("192.168.4.1", 37022), {
        t.ControlPacket: "transmit",
    })

    from modules.udp_bridge import UdpBridgeListener
    r.register_instance("telemetry_receiver", lambda: UdpBridgeListener(37021), {
        t.PollUpdate: "update"
    })

    from interface_modules.pygame_keyboard_input import PygameKeyboardInput
    r.register_instance("keyboard_control", lambda: PygameKeyboardInput(20), {
        t.PollUpdate: "update"
    })

    # ~ from interface_modules.pygame_camera_display import PygameCameraDisplay
    # ~ r.register_instance("camera_display", lambda: PygameCameraDisplay(camera_layer), {
        # ~ t.PollUpdate: "update"
    # ~ })
    from interface_modules.opencv_camera_display import OpenCVCameraDisplay
    r.register_instance("camera_display", OpenCVCameraDisplay, {
        t.PollUpdate: "update",
        t.SetCameraSurface: "set_draw_surface",
    })


    from interface_modules.telemetry_display import TelemetryDisplay
    r.register_instance("telemetry_display", TelemetryDisplay, {
        t.PollUpdate: "update",
        t.SetHudSurface: "set_draw_surface",
        t.Telemetry: "new_telemetry",
    })


def start():
    r.init_router(t.TOPIC_LIST)
    setup_modules()
    r.publish(t.StartInstance("router_log_bridge"))
    r.publish(t.StartInstance("log_printer"))

    log.info(
        "boot_info",
        {
            "python":sys.implementation.name,
            "platform": sys.platform,
            "type": "control_interface"
        }
    )

    r.publish(t.StartInstance("control_transmitter"))
    r.publish(t.StartInstance("telemetry_receiver"))
    r.publish(t.StartInstance("keyboard_control"))
    r.publish(t.StartInstance("camera_display"))
    r.publish(t.StartInstance("telemetry_display"))




def run():
    start_time = time.time()

    initial_size = (640, 480)
    window_flags = pygame.RESIZABLE | pygame.NOFRAME

    screen = pygame.display.set_mode(initial_size, flags=window_flags)
    camera_layer = pygame.Surface(initial_size)
    hud_layer = pygame.Surface(initial_size)

    while(1):
        now = time.time()
        delta_time = now - start_time
        start_time = now

        r.publish(t.PollUpdate(delta_time))
        r.update()
        screen.blit(camera_layer, (0,0))
        screen.blit(hud_layer, (0,0))

        hud_layer.fill(0)  # Clear the HUD layer

        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

            if event.type == pygame.VIDEORESIZE:
                surface = pygame.display.set_mode(event.size, window_flags)

                camera_layer = pygame.Surface(event.size, flags=pygame.SRCALPHA, depth=32)
                hud_layer = pygame.Surface(event.size, flags=pygame.SRCALPHA, depth=32)

                r.publish(t.ScreenResize(event.w, event.h))
                r.publish(t.SetCameraSurface(camera_layer))
                r.publish(t.SetHudSurface(hud_layer))


start()
run()
