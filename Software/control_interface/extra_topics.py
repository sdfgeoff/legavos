from collections import namedtuple
from topics import *


ScreenResize = namedtuple("ScreenResize", ['x', 'y'])
SetCameraSurface = namedtuple("SetCameraSurface", ['surface'])
SetHudSurface = namedtuple("SetHudSurface", ['surface'])



TOPIC_LIST = [
    globals()[topic_name]
    for topic_name in globals().keys()
    if hasattr(globals()[topic_name], "__name__")
]
